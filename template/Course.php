<?php
	$course		= get_queried_object();
	$icon_id 	= get_term_meta($course->term_id, 'icon', true);
	$logo 		= wp_get_attachment_image_src($icon_id, "full")[0];
	$author		= get_user_by("id", get_term_meta($course->term_id, 'author', true));
	if(count($posts) )
	{
		if( Bio_Course::is_locked( $course->term_id ))
		{
			$arch = "
			<div class='col-12 text-center'>
				<div class='alert alert-danger' role='alert'>".
					__("Matherials for Course are hidden", BIO).
				"</div>
			</div> ";
		}
		else
		{
			foreach($posts as $p)
			{
				$article	= Bio_Article::get_instance($p->ID);
				$arch 		.= $article->draw_arch();
			}			
		}
	}
	else
	{
		$arch = "
		<div class='alert alert-danger' role='alert'>".
			__("No article exists", BIO).
		"</div> ";
	}
	
	if(Bio_User::is_user_role("contributor"))
	{
		if( Bio_User::is_member_course( $course->term_id) )
		{
			$add = "
			<div class='alert alert-success' role='alert'>" . 
				__("You are a student in this course.", BIO) .
				"<div>
					<div class='btn btn-link leave_course text-center'  cid='$course->term_id'>" . 
						__("Request permission to leave the course", BIO) .
					"</div>
				</div>
			</div>" ;
		}
		else
		{
			if(Bio_Course::is_request_permission( $course->term_id ))
			{
				$add = Bio_Messages::alredy_request( $course->term_id );
			}
			else
			{
				$add = Bio_Messages::send_request( $course->term_id );				
			}
		}
	}
	else
	{
		$add = "
			<div class='alert alert-danger' role='alert'>".
				__("You cannot subscribe to the course.", BIO).
			"</div>";
	}
	$html .= "
	<div class='text-center'><div class='bio-course-icon-lg' style='background-image:url($logo);'></div></div>
	<h1  class='text-center'>". $course->name . "</h1>";
	$html .= "
	<div class='row'>
		<div class='col-12 bio_course_descr text-center'>
			<p>".
				$course->description .
			"</p>
			<p>".
				sprintf(__("Leader of Course - %s", BIO), "<B>".$author->display_name."</B>") .
			"</p>
			<p>".
				get_term_meta($course->term_id, 'adress', true) .
			"</p>
		</div>
		<div class='col-12 text-center' id='add'>
			$add
		</div>
		<hr/>
		$arch
	</div>";