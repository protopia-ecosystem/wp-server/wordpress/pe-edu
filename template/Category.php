<?php
	$course		= get_queried_object();
	if(count($posts) )
	{
		foreach($posts as $p)
		{
			$article	= Bio_Article::get_instance($p->ID);
			$arch 		.= $article->draw_arch();
		}
	}
	else
	{
		$arch = "
		<div class='alert alert-danger' role='alert'>".
			__("No article exists", BIO).
		"</div> ";
	}
	$html .= "
	<div class='text-center'><div class='bio-course-icon-lg' style='background-image:url($logo);'></div></div>
	<h1  class='text-center'>". $course->name . "</h1>";
	$html .= "
	<div class='row'>
		<div class='col-12 bio_course_descr text-center'>
			<p>".
				$course->description .
			"</p>
		</div>
		<hr/>
		$arch
	</div>";