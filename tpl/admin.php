<?php
	require_once(BIO_REAL_PATH."tpl/input_file_form.php");
	$int = get_option("mint");
	global $components;
	$components = [
		[ "article", 			__("Article", BIO) ],
		[ "page", 				__("Page", BIO) ],
		[ "category", 			__("Category", BIO) ],
		[ "bio_course",			__("Course", BIO) ],
		[ "bio_test", 			__("Test", BIO) ],
		[ "bio_event", 			__("Event", BIO) ],
		[ "bio_bology_theme", 	__("Biology Theme", BIO) ],
		[ "bio_olimpiad_type", 	__("Olimpiad Type", BIO) ],
	];
	function wp_bio_component($name, $selected)
	{
		global $components;
		$html = "<select class='form-control' name='$name'>";
		foreach( $components as $component)
		{
			$html .= "
			<option value='".$component[0]."' " . selected($component[0], $selected, 0) . ">".
				$component[1].
			"</option>";
		}
		$html .= "</select>";
		return $html;
	}
	$menu = "<div class='col-12 alert alert-info my-3 px-5 py-5'>".
		__("<h3>On each of 7 elements of Menu you may:</h3>
		<ul>
			<ol>on|off dislay this menu element</ol>
			<ol>put icon image for 45x45px</ol>
			<ol>get the name and choose distination. For that choose needed Component for display in distination page</ol>
			<ol>and choose method of filtering instances of this Component. Evalble single method (on intance by ID) and archive of all instance of Component.</ol>
		</ul>", BIO).
	"</div>";
	for($i=0; $i<7; $i++)
	{
		$menu .= "
		<div class='row d-flex align-items-center my-3'>
			
			<div class='col-1'>
				<input 
					type='checkbox' 
					class='checkbox ' 
					id='is_check$i' 
					name='menu_is_check[]' 
					value='1' 
					" . checked(1, static::$options['menu'][$i]['is_check'], 0) . " 
				/> 
				<label for='is_check$i'> </label>
			</div>
			<div class='col-2'>" .
				get_input_file_form2("", static::$options['menu'][$i]['icon'], "menu_icon$i", $i) .
				"
			</div>
			<div class='col-3'>
				<label>label</label>
				<p>
				<input type='text' name='menu_label[]' class='form-control' value='".static::$options['menu'][$i]['label']."' placeholder='" . __("Display label", BIO) . "' /> 
				<p>
				<label>uniq</label>
				<p>
				<input type='text' name='menu_uniq[]' class='form-control' value='".static::$options['menu'][$i]['uniq']."' placeholder='" . __("use latin and numbers for uniq name", BIO) . "' /> 
				
			</div>
			<div class='col-3'>
				<label>component</label>
				<p>" .
				wp_bio_component(
					"menu_component[]",
					static::$options['menu'][$i]['component']
				) .
			"</div>
			<div class='col-3'>
				<label>dop param</label>
				<p>
				<input type='text' name='menu_params[]' class='form-control' value='".static::$options['menu'][$i]['params']."' placeholder='" . __("param", BIO) . "' /> 
				<p>
				<label>is showed archive?</label>
				<p>
				<input 
					type='checkbox' 
					class='checkbox ' 
					id='menu_is_archive$i' 
					name='menu_is_archive[]' 
					value='1' 
					" . checked(1, static::$options['menu'][$i]['is_archive'], 0) . " 
				/> 
				<label for='menu_is_archive$i'> </label>
			</div>
		</div>";
	}
	$menu .= "";
	$html = "<style>
			.nav.nav-tabs 
			{
				padding: 0;
			}
		</style>
		<section>
			<div class='container'>
				<div class='row d-flex align-items-center'>
					<div class='col-md-1 critery_cell2'>
						<div class='bio-course-icon-lg' style='background-image:url(" . BIO_URLPATH . "assets/img/pe_edu_logo.svg);'></div>
					</div>
					<div class='col-md-11 critery_cell2'>
						<div class='display-4'> " . 
							__("PE Education Settings", BIO) . 
						"</div>
					</div>
				</div>
				<div class='spacer-10'></div>
				<nav>
				  <div class='nav nav-tabs' id='nav-tab' role='tablist'>
					<a class='nav-item nav-link active' id='nav-home-tab' data-toggle='tab' href='#nav-home' role='tab' aria-controls='nav-home' aria-selected='true'>".
						__("Main Settings", BIO).
					"</a>
					<!--a class='nav-item nav-link' id='nav-settings-tab' data-toggle='tab' href='#nav-settings' role='tab' aria-controls='nav-settings' aria-selected='false'>".
						__("Main menu settings", BIO).
					"</a-->
					<a class='nav-item nav-link' id='nav-utilities-tab' data-toggle='tab' href='#nav-utilities' role='tab' aria-controls='nav-utilities' aria-selected='false'>".
						__("Utilities", BIO).
					"</a>
				  </div>
				</nav>
				
				<div class='tab-content' id='nav-tabContent'>
				  <div class='tab-pane fade show active' id='nav-home' role='tabpanel' aria-labelledby='nav-home-tab'>				  
					<ul class='list-group'>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12'>".
									__("Page of Personal Cabinet", BIO).
								"</div>
								<div class='col-md-8 col-sm-12'>".
									wp_dropdown_pages( array(
										'selected'         => static::$options['cab_id'],
										'echo'             => false,
										'name'			   => "cab_id",
										'ID'			   => "cab_id",
										"class"			   => "form-control bio_options",
										"show_option_none" => "-"
									) ) .
									"<div class='spacer-10'></div>
								</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12'>".
									__("About Page", BIO).
								"</div>
								<div class='col-md-8 col-sm-12'>".
									wp_dropdown_pages( array(
										'selected'         => static::$options['about_id'],
										'echo'             => false,
										'name'			   => "about_id",
										'ID'			   => "about_id",
										"class"			   => "form-control bio_options",
										"show_option_none" => "-"
									) ) .
									"<div class='spacer-10'></div>
								</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12'>".
									__("Email", BIO).
								"</div>
								<div class='col-md-8 col-sm-12'>
									<input class='form-control bio_options' name='email' value='" .static::$options['email']. "' />
									<div class='spacer-10'></div>
								</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12'>".
									__("Adress", BIO).
								"</div>
								<div class='col-md-8 col-sm-12'>
									<input class='form-control bio_options' name='adress' value='" .static::$options['adress']. "' />
									<div class='spacer-10'></div>
								</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12'>".
									__("URL of unique web-client", BIO).
								"</div>
								<div class='col-md-8 col-sm-12'>
									<input type='url'  class='form-control bio_options' name='web_client_url' value='" .static::$options['web_client_url']. "' 
								</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='raw mb-4'>
								<div class='col-md-12 col-sm-12 mb-3 lead'>".
									__("Media taxonomies", BIO).
								"</div>
							</div>
							<div class='raw mb-4'>
								<div class='col-md-4 col-sm-12'>".
									__("Media Taxonomy for icons", BIO).
								"</div>
								<div class='col-md-8 col-sm-12'>".
									wp_dropdown_categories( [
										'show_option_all'    => '',
										'show_option_none'   => '---',
										'option_none_value'  => -1,
										'orderby'            => 'ID',
										'order'              => 'ASC',
										'echo'               => 0,
										'selected'           => static::$options['icon_media_term'],
										'hierarchical'       => 0,
										'name'               => 'icon_media_term',
										'id'                 => 'icon_media_term',
										'class'              => 'form-control bio_options',
										'taxonomy'           => BIO_MEDIA_TAXONOMY_TYPE,
										'hide_if_empty'      => false,
										'hide_empty'      	=> false,
									]).
								"</div>
							</div>
							<div class='raw mb-4'>
								<div class='col-md-4 col-sm-12'>".
									__("Media Taxonomy for tests", BIO).
								"</div>
								<div class='col-md-8 col-sm-12'>".
									wp_dropdown_categories( [
										'show_option_all'    => '',
										'show_option_none'   => '---',
										'option_none_value'  => -1,
										'orderby'            => 'ID',
										'order'              => 'ASC',
										'echo'               => 0,
										'selected'           => static::$options['test_media_term'],
										'hierarchical'       => 0,
										'name'               => 'test_media_term',
										'id'                 => 'test_media_term',
										'class'              => 'form-control bio_options',
										'taxonomy'           => BIO_MEDIA_TAXONOMY_TYPE,
										'hide_if_empty'      => false,
										'hide_empty'      	=> false,
									]) .
								"</div>
							</div>
							<div class='raw mb-4'>
								<div class='col-md-4 col-sm-12'>".
									__("Media Taxonomy for Users loaded", BIO).
								"</div>
								<div class='col-md-8 col-sm-12'>".
									wp_dropdown_categories( [
										'show_option_all'    => '',
										'show_option_none'   => '---',
										'option_none_value'  => -1,
										'orderby'            => 'ID',
										'order'              => 'ASC',
										'echo'               => 0,
										'selected'           => static::$options['test_media_free'],
										'hierarchical'       => 0,
										'name'               => 'test_media_free',
										'id'                 => 'test_media_free',
										'class'              => 'form-control bio_options',
										'taxonomy'           => BIO_MEDIA_TAXONOMY_TYPE,
										'hide_if_empty'      => false,
										'hide_empty'      	=> false,
									]).
								"</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-12 col-sm-12 mb-3 lead'>".
									__("Social links", BIO).
								"</div>
								<div class='col-md-4 col-sm-12'>".
									__("VK", BIO).
								"</div>
								<div class='col-md-8 col-sm-12 mb-2'>
									<input class='form-control bio_options mb-2' name='vk' value='" .static::$options['vk']. "' />
								</div>
								<div class='col-md-4 col-sm-12'>".
									__("Youtube", BIO).
								"</div>
								<div class='col-md-8 col-sm-12 mb-2'>
									<input class='form-control bio_options mb-2' name='youtube' value='" .static::$options['youtube']. "' />
								</div>
								<div class='col-md-4 col-sm-12'>".
									__("Android", BIO).
								"</div>
								<div class='col-md-8 col-sm-12 mb-2'>
									<input class='form-control bio_options mb-2' name='android' value='" .static::$options['android']. "' />
								</div>
								<div class='col-md-4 col-sm-12'>".
									__("Apple", BIO).
								"</div>
								<div class='col-md-8 col-sm-12 mb-2'>
									<input class='form-control bio_options mb-2' name='apple' value='" .static::$options['apple']. "' />
								</div>
							</div>
						</li>".
						apply_filters("bio_admin", "").
						"<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12 d-flex flex-column float-left'>".
									__("Default pictogramm", BIO).
									"<div>".
										get_input_file_form2("default_img",static::$options['default_img'],"default_img","") .
									"</div>
								</div>
								<div class='col-md-4 col-sm-12 d-flex flex-column'>".
									__("404 pictogramm", BIO).
									"<div>".
										get_input_file_form2("404", static::$options['404'], "404", "") .
									"</div>
								</div>
							</div>
						</li>
					</ul>				  
				  </div>
				  <div class='tab-pane fade' id='nav-settings' role='tabpanel' aria-labelledby='nav-settings-tab'>				  
					<ul class='list-group'>
						$menu
					</ul>
					<button type='button' class='btn btn-primary save_menu_setting'>" . __("Save all changes", BIO). "</button>
				</div>
				<div class='tab-pane fade' id='nav-utilities' role='tabpanel' aria-labelledby='nav-utilities-tab'>			  
					<ul class='list-group'>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-12 m-3'>".
									__("Export XML to Android", BIO).
									"<div class='my-3'>
										<div class='btn btn-danger' name='export_android'>".
											__("Start export to Android", BIO).
										"</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				
				<div class='spacer-10'></div>
				 <span id='is_save' style='vertical-align:top; display:inline-block; opacity:0;'>
					<svg aria-hidden='true' viewBox='0 0 512 512' width='40' height='40' xmlns='http://www.w3.org/2000/svg'>
						<path fill='green' d='M435.848 83.466L172.804 346.51l-96.652-96.652c-4.686-4.686-12.284-4.686-16.971 0l-28.284 28.284c-4.686 4.686-4.686 12.284 0 16.971l133.421 133.421c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-28.284-28.284c-4.686-4.686-12.284-4.686-16.97 0z'></path>
					</svg>
				</span>			
				
				
				
			</div>
		</section>";
		echo $html;