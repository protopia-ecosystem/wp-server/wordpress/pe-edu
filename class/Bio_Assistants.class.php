<?php 
/*
	Это класс генерации стандартных wordpress-страниц, не REACT
*/
require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
class Bio_Assistants
{
	static function init()
	{
		/*
		add_filter( 'bio_gq_options', 				[__CLASS__, 'bio_gq_options'], 9, 1);
		add_filter( 'bio_get_gq_options', 			[__CLASS__, 'bio_get_gq_options'], 9, 1);
		add_filter( 'bio_gq_options_input', 		[__CLASS__, 'bio_gq_options'], 9, 1);
		add_filter( 'bio_public_gq_options', 		[__CLASS__, 'bio_public_gq_options'], 9, 1);
		*/


		add_filter( 'template_include', 			[__CLASS__, 'my_template'], 9, 1);
		//add_filter( "ermak_body_script", 			[__CLASS__, "ermak_body_script"], 1);
		//add_filter( "ermak_body_before", 			[__CLASS__, "ermak_body_header"], 9);
		add_filter( "ermak_body_before", 			[__CLASS__, "ermak_body_content"], 50);
		//add_filter( "ermak_body_before", 			[__CLASS__, "ermak_body_footer"], 99);
		add_action( "pre_get_posts",				[__CLASS__, "pre_get_posts"]);
		add_filter( 'upload_mimes', 				[__CLASS__, 'upload_allow_types'] );
		//add_action( 'user_register', 				[__CLASS__, 'my_registration'], 10, 2 );
	}
	static function bio_gq_options( $arr ) 
	{
		$arr['goods_type'] = [ 
			'type' => Type::string(), 	
			'description' 	=> __( 'Paying goods', BIO )
		];
		
		return $arr;
	}
	static function bio_get_gq_options( $arr ) 
	{
		$arr['goods_type'] = Bio::$options['goods_type'];
		
		return $arr;
	}
	static function bio_public_gq_options( $arr ) 
	{
		
		$arr['goods_type'] = [ 
			'type' => PEGraphql::object_type("Label"), 	
			'description' 	=> __( 'Paying goods', BIO )
		];
		return $arr;
	}
	static function my_registration( $user_id ) 
	{
		// get user data
		$user_info = get_userdata($user_id);
		// create md5 code to verify later
		$code = md5(time());
		// make it into a code to send it to user via email
		$string = array('id'=>$user_id, 'code'=>$code);
		// create the activation code and activation status
		update_user_meta($user_id, 'account_activated', 0);
		update_user_meta($user_id, 'activation_code', $code);
		// create the url
		$url = get_site_url(). '/my-account/?act=' .base64_encode( serialize($string));
		// basically we will edit here to make this nicer
		$html = 'Please click the following links <br/><br/> <a href="'.$url.'">'.$url.'</a>';
		// send an email out to user
		wp_mail( $user_info->user_email, __('Email Subject','text-domain') , $html);
	}	
	
	
	
	static  function upload_allow_types( $mimes ) 
	{
		// разрешаем новые типы
		$mimes['svg']  = 'image/svg+xml';
		$mimes['doc']  = 'application/msword'; 
		// отключаем имеющиеся
		unset( $mimes['mp4a'] );
		return $mimes;
	}
	static function pre_get_posts($query)
	{
		if ( $query->is_front_page() && $query->is_main_query() ) 
		{
			$query->set("post_type", BIO_ARTICLE_TYPE );
		}
	}
	static function my_template($template)
	{
		global $post;
		return BIO_REAL_PATH."template/empty.php";
		return $template;
	}
	static function ermak_body_header( $text )
	{
		$html = "<div class='modal fade' id='bio_dialog' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
			<div class='modal-dialog modal-dialog-centered' role='document'>
				<div class='modal-content'>
					<div class='modal-header'>
						<h5 class='modal-title' id='exampleModalLabel'>Опаньки</h5>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
							<span aria-hidden='true'>&times;</span>
						</button>
					</div>
					<div class='modal-body'>
						Нихрена пока нет
					</div>
					<div class='modal-footer'>
						<button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
						<button type='button' class='btn btn-primary'>Save changes</button>
					</div>
				</div>
			</div>
		</div>	
			
		<div class='modal fade' id='bio_login' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
			<div class='modal-dialog modal-dialog-centered' role='document'>
				<div class='modal-content'>
					<div class='modal-header'>
						<h5 class='modal-title' id='exampleModalLabel'>Опаньки</h5>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
							<span aria-hidden='true'>&times;</span>
						</button>
					</div>
					<div class='modal-body'>
						".
							wp_login_form( array(
								'echo'           => false,
								'redirect'       => site_url( $_SERVER['REQUEST_URI'] ), 
								'form_id'        => 'loginform',
								'label_username' => __( 'Username' ),
								'label_password' => __( 'Password' ),
								'label_remember' => __( 'Remember Me' ),
								'label_log_in'   => __( 'Log In' ),
								'id_username'    => 'user_login',
								'id_password'    => 'user_pass',
								'id_remember'    => 'rememberme',
								'id_submit'      => 'wp-submit',
								'remember'       => true,
								'value_username' => NULL,
								'value_remember' => false 
							) )
						."
					</div>
				</div>
			</div>
		</div>	
			
		<div class='modal fade' id='bio_reg' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
			<div class='modal-dialog modal-dialog-centered' role='document'>
				<div class='modal-content'>
					<div class='modal-header'>
						<h5 class='modal-title' id='exampleModalLabel'>Опаньки</h5>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
							<span aria-hidden='true'>&times;</span>
						</button>
					</div>
					<div class='modal-body'>
						<form id='registerform' action='" . site_url('wp-login.php?action=register') . "' method='post'>
							<p>
								<label for='user_login'>
									Имя пользователя<br>
									<input type='text' name='user_login' id='user_login' class='input' value='' size='20' style=''>
								</label>
							</p>
							<p>
								<label for='user_email'>
									E-mail<br>
									<input type='email' name='user_email' id='user_email' class='input' value='' size='25'>
								</label>
							</p>

							<p id='reg_passmail'>Подтверждение регистрации будет отправлено на ваш e-mail.</p>

							<br class='clear'>
							<input type='hidden' name='redirect_to' value=''>

							<p class='submit'>
								<input type='submit' name='wp-submit' id='wp-submit' class='btn btn-primary' value='" . __("Register") . "'>
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>	
			
		<header>
			<nav class='navbar navbar-expand-lg navbar-dark bg-dark text-white'>
				<div class='container'> 
					<a class='navbar-brand' href='/'>" . __("pe-edu", BIO) . "</a>
					<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarNavDropdown' aria-controls='navbarNavDropdown' aria-expanded='false' aria-label='Toggle navigation'>
						<span class='navbar-toggler-icon'></span>
					</button>
					<div class='collapse navbar-collapse' id='navbarNavDropdown'>
						<ul class='navbar-nav'>
							<li class='nav-item active'>
								<a class='nav-link' href='#'>Home <span class='sr-only'>(current)</span></a>
							</li>
							<li class='nav-item'>
								<a class='nav-link' href='#'>Features</a>
							</li>
							<li class='nav-item'>
								<a class='nav-link' href='#'>Pricing</a>
							</li>
						</ul>
					
					</div>
					<div class='bio_cab btn-group' role='group'>".
						static::get_cabunet_form().
					"</div>
				  </div>
			</nav>
		</header>";
		
		return $text.$html;
	}
	static function ermak_body_content( $text )
	{
		global $posts, $post, $wp_query ;
		//var_dump( $wp_query );
		//var_dump(get_queried_object());
		//wp_die();
		$html = "
		<section class='content'>
			<div class='container'>
				<div class='row'>
					<div class='col-md-12'>";
		$get_queried_object = get_queried_object();
		$taxonomy = $get_queried_object ? $get_queried_object->taxonomy : null;
		switch(true)
		{
			case is_tax(BIO_COURSE_TYPE):
				require_once BIO_REAL_PATH . "template/Course.php";				
				break;
			case $taxonomy == 'category':
				require_once BIO_REAL_PATH . "template/Category.php";				
				break;
			case is_front_page():
				/*
				foreach($posts as $post)
				{
					if($post->post_type == BIO_ARTICLE_TYPE)
					{
						$article	= Bio_Article::get_instance($post->ID);
						$html 		.= $article->draw_arch();
					}
				}	
				*/
				wp_editor('content', 'editor_id', array(
					'wpautop'       => 1,
					'media_buttons' => 1,
					'textarea_name' => 'text', //нужно указывать!
					'textarea_rows' => 20,
					'tabindex'      => null,
					'editor_css'    => '',
					'editor_class'  => '',
					'teeny'         => 0,
					'dfw'           => 0,
					'tinymce'       => 1,
					'quicktags'     => 1,
					'drag_drop_upload' => false
				) );
				break;
			case $post->post_type == BIO_EVENT_TYPE && $wp_query->is_main_query():
				$event = Bio_Event::get_instance($post->ID);
				$html .= "
				
				<div class='row bio_test_content' id='bio_test_content'>					
					<nav aria-label='breadcrumb'>
						<ol class='breadcrumb'>
							<li class='breadcrumb-item'>
								<a href='/'>".
									__("Main") .
								"</a>
							</li>
							<li class='breadcrumb-item active' aria-current='page'>
								<i class='fas fa-caret-right'></i>
								<span> ".
									$event->get("post_title").
								"</span>
							</li>
						</ol>
					</nav>" .
					$event->draw( ) .					
				"</div>";
				break;
			case $post->post_type == BIO_TEST_TYPE && $wp_query->is_main_query():
				$test = Bio_Test::get_instance($post->ID);
				$html .= "
				
				<div class='row bio_test_content' id='bio_test_content'>					
					<nav aria-label='breadcrumb'>
						<ol class='breadcrumb'>
							<li class='breadcrumb-item'>
								<a href='/'>".
									__("Main") .
								"</a>
							</li>
							<li class='breadcrumb-item'>
								<i class='fas fa-caret-right'></i>
								<a href='" . get_permalink($test->get_article()->ID) . "'>".
									$test->get_article_title() .
								"</a>
							</li>
							<li class='breadcrumb-item active' aria-current='page'>
								<i class='fas fa-caret-right'></i>
								<span> ".
									$test->get("post_title").
								"</span>
							</li>
						</ol>
					</nav>" .
					$test->draw( ) .					
				"</div>";
				break;
			case $post->post_type == BIO_ARTICLE_TYPE && $wp_query->is_main_query():
				$article = Bio_Article::get_instance($post->ID);
				$html .= "
				
				<div class='row'>" .
					$article->draw( ) .					
				"</div>";
				break;
			case $post->ID == @Bio::$options['cab_id']:
				require_once(BIO_REAL_PATH . "tpl/Cabinet.php");
				break;
			default:
				
				break;
		}		
		$html .= "
					</div>
				</div>
			</div>
		</section>";
		
		return $text.$html;
	}
	static function ermak_body_footer( $text )
	{
		$html = "
		<footer class=' bio_footer bg-dark text-white'>
			<div class='container'>
				<div class='row'>
					<div class='col-md-4 col-12 bio_footer-widget'>					
						<div><div class='bio_footer_title'>1</div></div>
					</div>
					<div class='col-md-4 col-12 bio_footer-widget'>					
						<div><div class='bio_footer_title'>2</div></div>
					</div>
					<div class='col-md-4 col-12 bio_footer-widget right'>					
						<div><div class='bio_footer_title'>3</div></div>
					</div>
				</div>
			</div>
		</footer>";
		
		return $text.$html;
	}
	
	static function get_cabunet_form()
	{
		if(is_user_logged_in())
		{
			$html = "<a class='btn btn-outline-warning bio_cabinet text-left' href='".get_permalink(Bio::$options["cab_id"])."'>
						<i class='fa fa-user'></i> " . wp_get_current_user()->display_name. "
					</a>
					<a class='btn btn-outline-warning bio_logout' href='" . wp_logout_url('/') . "'>
						<i class='fas fa-sign-out-alt' title='" . __("Logout") . "'></i>
					</a>";
		}
		else
		{
			$html = "<div class='btn btn-outline-warning bio_login text-left' data-toggle='modal' data-target='#bio_login'>
						<i class='fa fa-user'></i> Log in
					</div>
					<div class='btn btn-outline-warning bio_login text-left' data-toggle='modal' data-target='#bio_reg'>
						<i class='fa fa-user'></i> Register
					</div>";
		}
		return $html;
	}
	static function insert_media( $data, $post_id=0 )
	{
		if(!function_exists('wp_handle_upload')){
			require_once ABSPATH . 'wp-admin/includes/image.php';
			require_once ABSPATH . 'wp-admin/includes/file.php';
			require_once ABSPATH . 'wp-admin/includes/media.php';
		}
		// get rid of everything up to and including the last comma
		$imgData1 = $data['data'];
		$imgData1 = substr($imgData1, 1 + strrpos($imgData1, ','));
		// write the decoded file
		$filePath	= BIO_REAL_PATH. 'temp/' . $data['media_name'];
		$fileURL	= BIO_URLPATH  . 'temp/' . $data['media_name'];
		file_put_contents($filePath, base64_decode($imgData1));
		
		
		//$id = media_sideload_image( $fileURL, 0, $data['media_name'], "id" );
		$desc = $data['media_name'];
		$file_array = [];

		$tmp = download_url( $fileURL );
		// корректируем умя файла в строках запроса.
		preg_match('/[^?]+\.(jpg|jpe|jpeg|gif|png|svg)/i', $fileURL, $matches);
		$file_array['name'] = $data['media_name'];// basename($matches[0]);
		$file_array['tmp_name'] = $tmp;
		$id = media_handle_sideload( $file_array, $post_id, $data['media_name'] );

		// удалим временный файл
		@unlink( $file_array['tmp_name'] );
		@unlink( $filePath );		
		
		$url = wp_get_attachment_url( $id );
		return ["url"=> $url, "id" => $id];
	}
	static function extract_img($content, $title, $post_id=0)
	{
		return $content;
		mb_internal_encoding("UTF-8");
		$doc = new DOMDocument();
		$doc->strictErrorChecking = false;
		@$doc->loadHTML($content);
		
		$tags = $doc->getElementsByTagName('img');
		$d = [];
		foreach ($tags as $tag) 
		{
				$prefix = substr($tag->getAttribute('src'), 0,5);
				if($prefix == "data:")
				{
					//$d[] = $tag->getAttribute('src');
					$data = [ "data" => $tag->getAttribute('src'), "media_name" => $title.".jpg"];
					$img = Bio_Assistants::insert_media( $data, $post_id );
					wp_set_object_terms( $img['id'], (int)Bio::$options['test_media_free'], BIO_MEDIA_TAXONOMY_TYPE );
					$tag->setAttribute("src", $img['url']);
				}				
		}
		$html =  $doc->saveHTML( );
		$html = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"', '', $html);
		$html = str_replace('"http://www.w3.org/TR/REC-html40/loose.dtd">', '', $html);
		$html = str_replace('<html><body>', '', $html);
		$html = str_replace('</body></html>', '', $html);
		return $html;
	}
}