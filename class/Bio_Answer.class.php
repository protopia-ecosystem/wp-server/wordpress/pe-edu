<?php

class Bio_Answer extends SMC_Post
{
	static function get_type()
	{
		return BIO_ANSWER_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 2);	
		//add_action('admin_menu',				[__CLASS__, 'my_extra_fields2']);
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Answer", BIO), // Основное название типа записи
			'singular_name'      => __("Answer", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Answer", BIO), 
			'all_items' 		 => __('Answers', BIO),
			'add_new_item'       => __("add Answer", BIO), 
			'edit_item'          => __("edit Answer", BIO), 
			'new_item'           => __("add Answer", BIO), 
			'view_item'          => __("see Answer", BIO), 
			'search_items'       => __("search Answer", BIO), 
			'not_found'          => __("no Answers", BIO), 
			'not_found_in_trash' => __("no Answers in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Answers", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 3,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array('title', 'thumbnail'),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
	
	static function add_views_column( $columns )
	{
		$posts_columns 			= parent::add_views_column( $columns ); 
		$posts_columns['picto'] = __("Image");
		return $posts_columns;			
	}
	static function fill_views_column($column_name, $post_id) 
	{
		switch($column_name)
		{
			case "picto":
				echo "<div class='bio-course-icon-lg' style='background-image:url(".get_the_post_thumbnail_url( $post_id, [100,100] ).")' ></div>";
				break;
			default:
				parent::fill_views_column($column_name, $post_id);
		}
	}
	static function get_admin_stroke( $id=-1, $i = -1 , $is_hidden=true)
	{
		$answer		= static::get_instance($id);
		$icon_id	= get_post_thumbnail_id( $answer->id );
		$html ="<li class='" . ($is_hidden ? "bio-hidden" : "") . "' ans_num='$i' >
				<div class='row'>
					<div class='col-8'>
						<input type='text' name='answer_text[$i]' value='".
							$answer->get("post_title") . 
						"' class='form-control' />
					</div>
					<div class='col-2'>".
						get_input_file_form5( "ans_icon", $icon_id, "answer_thumbnail", $i ) .
					"</div>
					<div class='col-1'>
						<label class='_check_'>
							<input name='answer_is_right[$i]' type='checkbox' value='1' ".checked(1,$answer->get_meta("is_right"),0)."/>
						</label>
					</div>
					<div class='col-1 text-right'>
						<div class='btn btn-outline-secondary btn-sm bio_close_answer' >
							<i class='fas fa-times'></i>
						</div>
					</div>
					<div class=spacer-5/></div>
					<input name='answer_id[$i]' type='hidden' value='" . $answer->id . "' />
				</div>
			</li>";
		return $html;
	}
}