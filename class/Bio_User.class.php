<?php

require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class Bio_User
{
	static function init()
	{	
		add_filter( 'pe_graphql_get_user', 			[__CLASS__, 'pe_graphql_get_user' ],9);		
		add_filter( 'pe_graphql_change_user', 		[__CLASS__, 'pe_graphql_change_user' ],10, 2);		
		add_action( 'show_user_profile', 			[__CLASS__, 'extra_user_profile_fields' ],9);		
		add_action( 'edit_user_profile', 			[__CLASS__, 'extra_user_profile_fields' ],9);
		add_action( 'edit_user_profile_update', 	[__CLASS__, 'edit_user_profile_update' ],9, 2); 
		add_filter('manage_users_columns' , 		[__CLASS__, 'add_extra_user_column'], 2);	
		add_action('manage_users_custom_column',	[__CLASS__, 'user_column_content'], 99, 3);
		add_action('pe_graphql_change_meta_user',	[__CLASS__, 'pe_graphql_change_meta_user'], 99, 2);
		add_action("pe_graphql_make_schema", 		[__CLASS__, "exec_graphql"], 8);
	}
	static function exec_graphql()
	{
		try
		{
			static::register_gq( );
		}
		catch(Bio_GraphQLNotAccess $ew)
		{
			
		}
		catch(Bio_GraphQLNotLogged $ew)
		{
			
		}
		catch(Bio_GraphQLNotAdmin $ew)
		{
			
		}
		catch(Bio_GraphQLError $ew)
		{
			
		}
	}
	static function register_gq()
	{
		PEGraphql::add_query( 
			'getFullUserBlockedCount', 
			[
				'description' 		=> __( "Get count of all blocked users", BIO ),
				'type' 				=> Type::int(),
				'args'     			=> [ ],
				'resolve' 			=> function( $root, $args, $context, $info )
				{
					global $wpdb;	 
					$users = get_users([
						"fields"	=> "ID",
						'meta_query' => [
							'relation' => 'OR',
							[
								'key' => 'is_blocked',
								'value' => '1'
							] 
						]
					]);
					return count($users);
				}
			] 
		);
	}
	static function pe_graphql_change_user( $user_data, $id )
	{
		update_user_meta($id, "user_descr", $user_data["user_descr"]);
		//wp_die( $id . " -- ". get_user_meta($id, 'user_descr', true) );
		return $user_data;
	}
	static function pe_graphql_get_user( $user )
	{
		//$user->user_descr = get_user_meta($user->ID, "user_descr", true);
		return $user;
	}
	static function extra_user_profile_fields( $user )
	{
		$account_activated = get_user_meta($user->ID, "account_activated", true);
		echo '
			<table class="form-table clear_table1">
				<tr>
					<th>
						<label>'. __("Is verified e-mail", BIO).'</label>
					</th>
					<td>
						<input type="checkbox" class="checkbox" id="$account_activated'.$user->ID.'" name="$account_activated" value="1" '.checked($account_activated, 1, 0). '/>
						<label for="$account_activated'.$user->ID.'" />
					</td>
				</tr>
				<tr>
					<th>
						<label>'. __("Roles", BIO).'</label>
					</th>
					<td>'.
						static::role_group_iface( $user->ID ) .
					'</td>
				</tr>
			</table>
		';
		//$user->display_name;
	}
	static function edit_user_profile_update($user_id )
	{
		update_user_meta($user_id, "account_activated", $_POST['account_activated']);
		//update_user_meta($user_id, "user_descr", $data['user_descr']);
	}
	static function get_full_count($params= 1)
	{
		if(!is_array($params)) $params = [];
		
		$args = [
			'orderby'   => 'display_name',
			'order'      => 'ASC',
		];
		if($params['class'])
		{
			$args['meta_query'] = [];
			$args['meta_query'][] = ['key' => "studentclass", "value" => $params['class'], "operator" => "OR"];
		}
		if($params['role__in'])
		{
			$args['role__in'] = $params['role__in'];
		}
		if($params['include'])
		{
			$args['include'] = $params['include'];
		}
		if($params['search'])
		{
			$args['search'] = "*".$params['search']."*";
		}
		$users = get_users( $args );
		return count($users);
	}
	static function get_all2($params= 1, $only_args = false)
	{
		if(!is_array($params)) $params = [];
		if(!isset($params["numberposts"]))
			$params["numberposts"] = 10;
		if(!isset($params["offset"]))
			$params["offset"] = 0;
		
		$args = [
			"offset" 	=> $params["offset"],
			"number"	=> $params["numberposts"],
			'orderby'   => 'display_name',
			'order'      => 'ASC',
		];
		if($params['class'])
		{
			$args['meta_query'] = [];
			$args['meta_query'][] = ['key' => "studentclass", "value" => $params['class'], "operator" => "OR"];
		}
		if($params['role__in'])
		{
			$args['role__in'] = $params['role__in'];
		}
		if($params['include'])
		{
			$args['include'] = $params['include'];
		}
		if($params['search'])
		{
			$args['search'] = "*".$params['search']."*";
		}
		$args['fields'] = $params['fields'] ? [$params['fields']] : 'all';
		$users = apply_filters("bio_get_users", get_users( $args ), $params);
		$result	= [];
		foreach($users as $re)
		{
			$av = (string)get_user_meta($re->ID, "avatar", true);
			$result[] = [
				"ID"			=> $re->ID,
				"display_name" 	=> $re->display_name,
				"user_registered" => date_i18n( 'j M Y, H:m', strtotime( $re->user_registered)),
				"user_email"	=> $re->user_email,
				"rules"			=> $re->roles,
				"studentclass"	=> get_term_by("id", get_usermeta($re->ID, "studentclass", true), BIO_CLASS_TYPE)->term_id,
				"avatar"		=> $av ? get_bloginfo("url").$av : ""
			];
		}
		return $only_args ? $args : $result;
	}

	static function get_all($params= 1)
	{
		if(!is_array($params)) $params = [];
		if(!isset($params["numberposts"]))
			$params["numberposts"] = 10;
		if(!isset($params["offset"]))
			$params["offset"] = 0;
		global $wpdb;
		$query = "SELECT u.ID, u.display_name, u.user_email, um.meta_value AS rules FROM " . $wpdb->prefix . "users AS u
		LEFT JOIN ".$wpdb->prefix . "usermeta AS um ON um.user_id=u.ID 
		WHERE um.meta_key='" . $wpdb->prefix . "capabilities'
		LIMIT " . ($params["offset"] * $params["numberposts"]) . ", " . $params["numberposts"] . "; ";
		$res = $wpdb->get_results($query);
		$result	= [];
		foreach($res as $re)
		{
			$rss = [];
			foreach($re as $key=>$val)
			{
				$rss[$key] = $key == "rules" ? array_keys( unserialize( $val ) ) : $val;			
			}
			$result[] = $rss;
		}
		return $result;
	}
	static function add_extra_user_column($columns)
	{
		unset($columns['role']);
		$columns['roles'] = __("Roles", BIO);
		$columns['params'] = __("Parameters", BIO);
		return $columns;
	}
    static function parse_classes($cls, $is_set = true)
    {
        $classes= [];
        $all 	= Bio_Class::get_all();
        $i 		= 0;
        foreach($all as $class)
        {
            if($cls[$i])
                $classes[] = $is_set ? $class->term_id : $class;
            $i++;
        }
        return $classes;
    }
    static function parse_rules( $cls )
    {
        $rules = [];
        $all = get_full_bio_roles();
        foreach( $cls as $r)
        {
             $rules[] = $all[$r][0];
        }
        return $rules;
    }

    public static function api_action($type, $methods, $code, $pars, $user)
    {
		global $wpdb, $ggggg;
        if(!static::is_user_role("administrator", $user->ID)) throw new ExceptionNotLoggedREST();
        $users	= [];
        switch($methods) {
            case "update":
                if(is_numeric($code)) {
                    Bio_User::update($pars, $code, true);
                    $update = 'success';
                }else{
                    $update = 'error';
                }
                break;
            case "delete":
                $update = 'error';
                break;
            case "create":
                if(is_numeric($code)) {

                    Bio_User::update($pars, $code, true);

                    $update = 'success';
                }else{

                    $update = 'error';
                }
                break;
            case "read":
            default:
                if(is_numeric($code))
				{
                    $users[]			= Bio_User::to_rest($code);
					$msg = __("You enter by ", BIO);
                }
				else
				{

                    //if(!current_user_can("manage_options"))	throw new ExceptionNotAdminREST();
                    $offset			 = isset($pars['offset'])			? (int) $pars['offset']		: 0;
                    $numberposts	 = ($pars['numberposts'] > 0 ) ? (int) $pars['numberposts'] : 10;
                    $searh			 = $pars['searh'];
					if(is_array( $pars["current_bio_classes"] ))
						$class		= $pars["current_bio_classes"];//static::parse_classes($pars["current_bio_classes"]);
                    if(count($pars["current_rules"]) > 0)
						$rule	= static::parse_rules($pars["current_rules"]);
                    if($pars["current_bio_tests"] > 0 && $pars["credits"] && is_array($pars["credits"]))
					{
						$query = "SELECT DISTINCT user_id 
							FROM `bio_result` 
							WHERE test_id=" . $pars['current_bio_tests'] . " 
							AND credits>".$pars['credits'][0]." AND credits<".$pars['credits'][1].";";
						$results = $wpdb->get_results($query);
						$u = [];
						foreach($results as $res)
							$u[] = $res->user_id;
						$include = count($u) ? $u : [ 0 ];
						$ggggg = $include;
					}					
					
                    $users = Bio_User::get_all2([
                        "numberposts" 	=> $numberposts,
                        'offset' 		=> $offset * $numberposts,
                        "search"		=> $searh,
                        "class"			=> $class,
                        "role__in"		=> $rule,
						"include"		=> $include
                    ], false);

                    $count = Bio_User::get_full_count([
                        "numberposts" 	=> $numberposts,
                        'offset' 		=> $offset * $numberposts,
                        "search"		=> $searh,
                        "class"			=> $class,
                        "role__in"		=> $rule,
						"include"		=> $include
                    ]);

                }
        }
        return Array (
            "users" => $users,
            "count" => $count,
            "offset" => $offset,
            "msg" => $msg,
            "numberposts" => $numberposts,
            "id" => $code,
            "update"=> $update
        );
    }

    function user_column_content($value, $column_name, $user_id)
	{
		switch($column_name )
		{
			case "params":
				
				break;
			case "roles":
				return static::role_group_iface( $user_id );
		}
		return $value;
	}

    static function delete( $post_id )
    {
        $post_id = (int)$post_id;
        return $post_id;
    }

	static function role_group_iface( $user_id )
	{
		$html = "
				<section>
					<div class='row'>
						<input type='checkbox' class=checkbox id='is_admin_$user_id' value='1' " . checked(1, (int)static::is_user_role("administrator", $user_id), 0) . " role_user_id='$user_id' role='administrator'/>		<label for='is_admin_$user_id'>".__("Administrator")."</label>
					</div>
					<div class='spacer-10'></div>";
				foreach(get_bio_roles() as $r)
				{
					//if( in_array($r, FmRU::$options['fmru_evlbl_roles'])) 
						$html .= "<div class='row'>
							<input type='checkbox' class=checkbox id='is_".$r[0]."_$user_id' value='1' " . checked(1, (int)static::is_user_role($r[0] , $user_id), 0) . " role_user_id='$user_id' role='".$r[0]."'/>		
							<label for='is_".$r[0]."_$user_id'>".__( $r[1], BIO )."</label>
						</div>
						<div class='spacer-10'></div>";
				}
				$html .= "
					<div class='row'>
						<input type='checkbox' class=checkbox id='is_editor_$user_id' value='1' " . checked(1, (int)static::is_user_role("editor", $user_id), 0) . " role_user_id='$user_id' role='editor'/>		<label for='is_editor_$user_id'>".__("Editor")."</label>
					</div>
					<div class='spacer-10'></div>
					<div class='row'>
						<input type='checkbox' class=checkbox id='is_contributor_$user_id' value='1' " . checked(1, (int)static::is_user_role("contributor", $user_id), 0) . " role_user_id='$user_id' role='contributor'/>		<label for='is_contributor_$user_id'>".__("Contributor")."</label>
					</div>
					<div class='spacer-10'></div>";
				return $html;
	}
    static function pe_graphql_change_meta_user( $id, $data )
	{
		return static::update( $data, $id, false );
	}
    static function update( $data, $post_id, $is_admin=false )
    {

        $user_id 	= (int)$post_id;
		$user		= get_userdata( $user_id );
        if($data['psw'] && $is_admin==false)
        {
            wp_update_user([
                "ID"		=> $user_id,
                "user_login"=> $data["email"],
                "user_email"=> $data["email"],
                "display_name"=> $data['fname']." ".$data['sname'],
                "first_name"=> $data['fname'],
                "last_name" => $data['sname'],
                "user_pass" => $data['psw']
            ]);
		}
		//wp_die( $data );

		if($data['avatar_name'])
		{
			if(!function_exists('wp_handle_upload')){
				require_once ABSPATH . 'wp-admin/includes/image.php';
				require_once ABSPATH . 'wp-admin/includes/file.php';
				require_once ABSPATH . 'wp-admin/includes/media.php';
			}
			// get rid of everything up to and including the last comma
			$imgData1 	= $data['avatar'];
			$imgData1 	= substr($imgData1, 1 + strrpos($imgData1, ','));
			// write the decoded file
			$ext 		= substr($data['avatar_name'], strrpos($data['avatar_name'], ".")+1);
			$nm 		= $user_id . "." . $ext;
			$filePath	= ABSPATH. '/avatars/' . $nm ;
			$fileURL	= get_bloginfo("url")  . '/avatars/' . $nm;
			file_put_contents($filePath, base64_decode($imgData1));
			update_user_meta($user_id, "avatar", '/avatars/' . $nm );
		}
        update_user_meta($user_id, "mname", $data['mname']);
        //update_user_meta($user_id, "user_descr", $data['user_descr']);
        update_user_meta($user_id, "birthday", $data['birthday']);
        update_user_meta($user_id, "phonenumber", $data['phonenumber']);
        update_user_meta($user_id, "anover", $data['anover']);
        update_user_meta($user_id, "graduationyear", $data['graduationyear']);
        update_user_meta($user_id, "parentphonename1", $data['parentphonename1']);
        update_user_meta($user_id, "parentphonename2", $data['parentphonename2']);
        update_user_meta($user_id, "parentphonenumber2", $data['parentphonenumber2']);
        update_user_meta($user_id, "parentphonenumber1", $data['parentphonenumber1']);
        update_user_meta($user_id, "usergroup", $data['usergroup']);
        update_user_meta($user_id, "school", $data['school']);
        update_user_meta($user_id, "studentclass", $data['studentclass']);
        update_user_meta($user_id, "town", $data['town']);
        update_user_meta($user_id, "gender", $data['gender']);
		if($data['usergroup'] == "teacher")
		{
			$user->add_role("Teacher");
		}
		else
		{
			$user->remove_role("Teacher");
		}
		if($data['usergroup'] == "anover")
		{
			$user->add_role("contributor");
		}
		else
		{
			$user->remove_role("contributor");
		}
		if($data['usergroup'] == "pupil")
		{
			$user->add_role("Pupil");
		}
		else
		{
			$user->remove_role("Pupil");
		}
        return $post_id;
	}
	
	static function createGUID() { 
    
		// Create a token
		$token      = $_SERVER['HTTP_HOST'];
		$token     .= $_SERVER['REQUEST_URI'];
		$token     .= uniqid(rand(), true);
		
		// GUID is 128-bit hex
		$hash        = strtoupper(md5($token));
		
		// Create formatted GUID
		$guid        = '';
		
		// GUID format is XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX for readability    
		$guid .= substr($hash,  0,  8) . 
			 '-' .
			 substr($hash,  8,  4) .
			 '-' .
			 substr($hash, 12,  4) .
			 '-' .
			 substr($hash, 16,  4) .
			 '-' .
			 substr($hash, 20, 12);
				
		return $guid;
	}	

    static function insert( $data )
    {
        $post_id = null;


        $answ = wp_insert_user([
            "user_login"	=> static::createGUID(),
            "user_pass"		=> $data["password"],
            "user_email"	=> $data["user_email"],
            "first_name"	=> $data["first_name"],
            "last_name"		=> $data["last_name"],
			"role"			=> "contributor"
        ]);
		if(!is_wp_error( $answ))
		{
			/*
			$user_id = $answ;
			
			if($data['avatar_name'])
			{
				if(!function_exists('wp_handle_upload')){
					require_once ABSPATH . 'wp-admin/includes/image.php';
					require_once ABSPATH . 'wp-admin/includes/file.php';
					require_once ABSPATH . 'wp-admin/includes/media.php';
				}
				// get rid of everything up to and including the last comma
				$imgData1 	= $data['avatar'];
				$imgData1 	= substr($imgData1, 1 + strrpos($imgData1, ','));
				// write the decoded file
				$ext 		= substr($data['avatar_name'], strrpos($data['avatar_name'], ".")+1);
				$nm 		= $user_id . "." . $ext;
				$filePath	= ABSPATH. '/avatars/' . $nm ;
				$fileURL	= get_bloginfo("url")  . '/avatars/' . $nm;
				file_put_contents($filePath, base64_decode($imgData1));
				update_user_meta($user_id, "avatar", '/avatars/' . $nm );
			}
			update_user_meta($user_id, "mname", $data['mname']);
			update_user_meta($user_id, "birthday", $data['birthday']);
			update_user_meta($user_id, "phonenumber", $data['phonenumber']);
			update_user_meta($user_id, "anover", $data['anover']);
			update_user_meta($user_id, "graduationyear", $data['graduationyear']);
			update_user_meta($user_id, "parentphonename1", $data['parentphonename1']);
			update_user_meta($user_id, "parentphonename2", $data['parentphonename2']);
			update_user_meta($user_id, "parentphonenumber2", $data['parentphonenumber2']);
			update_user_meta($user_id, "parentphonenumber1", $data['parentphonenumber1']);
			update_user_meta($user_id, "usergroup", $data['usergroup']);
			update_user_meta($user_id, "school", $data['school']);
			update_user_meta($user_id, "studentclass", $data['studentclass']);
			update_user_meta($user_id, "town", $data['town']);
			update_user_meta($user_id, "gender", $data['gender']);
			$user 		= get_userdata($user_id);
			switch( $data['usergroup'] )
			{
				case "teacher":
					$user->add_role("Teacher");
					break;
				case "pupil":
					$user->add_role("Pupil");
					break;
				default:
					$user->add_role("contributor");
					break;
			}	
			*/			
		}
        return $answ;
    }
	static function get_caps($user_id =-1)
	{
		$len = 30;
		$roles = static::get_roles($user_id);
		$caps = [0,0,0,0,0,0,0,0];
		$num = (int)( $capability / $len );
		foreach($roles as $role)
		{
			$caps[0] |= Bio::$options['caps'][$role][0];			
			$caps[1] |= Bio::$options['caps'][$role][1];			
			$caps[2] |= Bio::$options['caps'][$role][2];			
			$caps[3] |= Bio::$options['caps'][$role][3];			
			$caps[4] |= Bio::$options['caps'][$role][4];			
			$caps[5] |= Bio::$options['caps'][$role][5];			
			$caps[6] |= Bio::$options['caps'][$role][6];			
			$caps[7] |= Bio::$options['caps'][$role][7];			
		}
		return $caps;
	}
	
	static function access_caps_gq($capability, $msg)
	{
		if( ! Bio_User::is_user_cap($capability) )
			throw new \Bio_GraphQLNotAccess( $msg );
	}
	static function is_user_cap($capability, $user_id =-1)
	{
		$user_id = $user_id > 0 ? $user_id : get_current_user_id();
		$roles = static::get_roles($user_id);
		$caps = false;
		$caps2 = [];
		foreach($roles as $role)
		{
			$caps  = $caps || static::is_cap($capability, $role);	 
			$caps2[]  = static::is_cap($capability, $role);	 
		}
		return $caps;
	}
	static function is_cap($capability, $role)
	{
		if( static::is_user_roles(['administrator']) ) return true;
		$len = 30;
		$num = (int)( $capability / $len );
		//return [$num, Bio::$options['caps'][$role][$num], pow( 2, $capability % $len ) ];
		return ( Bio::$options['caps'][$role][$num] & pow( 2, $capability % $len ) ) > 0;
	}
    static function to_rest($p=-1)
    {
        $c = [];
        if(is_numeric( $p ) && $p > 0)
        {
            $user_id 	= $p;
            $suser 		= get_userdata($user_id);
            $c['email'] = $suser->user_email;
            $c['caps'] 	= static::get_caps( $user_id );
            /*
			$c['caps_examples'] = [
				"BIO_BIOLOGY_THEME_CREATE" 	=> Bio_User::is_user_cap(BIO_BIOLOGY_THEME_CREATE),
				"BIO_ARTICLE_CREATE" 		=> Bio_User::is_user_cap(BIO_ARTICLE_CREATE),
				"BIO_ARTICLE_ADD_COURSE" 	=> Bio_User::is_user_cap(BIO_ARTICLE_ADD_COURSE),
				"BIO_MAILING_CREATE" 		=> Bio_User::is_user_cap(BIO_MAILING_CREATE),			
			];
			*/
            $c['display_name'] = $suser->display_name;
            $c['first_name']	= $suser->first_name;
            $c['last_name']	= $suser->last_name;
            $c['user_registered']	= date_i18n( 'j M Y, H:m', strtotime( $suser->user_registered ) );

            $c['fname']	= $suser->first_name;
            $c['sname']	= $suser->last_name;

            $avatar			= (string)get_user_meta($user_id, "avatar", true);
			$c['avatar']	= $avatar? get_bloginfo("url").$avatar : "/assets/img/user_icon3.svg";
            $c['mname']			                = (string)get_user_meta($user_id, "mname", true);
            $c['birthday']			            = (string)get_user_meta($user_id, "birthday", true);
            $c['phonenumber']			        = (string)get_user_meta($user_id, "phonenumber", true);
            $c['parentphonename1']	     		= (string)get_user_meta($user_id, "parentphonename1", true);
            $c['parentphonename2']	        	= (string)get_user_meta($user_id, "parentphonename2", true);
            $c['parentphonenumber2']	        = (string)get_user_meta($user_id, "parentphonenumber2", true);
            $c['parentphonenumber1']	        = (string)get_user_meta($user_id, "parentphonenumber1", true);
            $c['usergroup']			            = (string)get_user_meta($user_id, "usergroup", true);
            $c['school']			            = (string)get_user_meta($user_id, "school", true);
            $c['graduationyear'] 		        = (string)get_user_meta($user_id, "graduationyear", true);
            $c['studentclass'] 		            = (string)get_user_meta($user_id, "studentclass", true);
            $c['town'] 		                    = (string)get_user_meta($user_id, "town", true);
            $c['gender']			            = (string)get_user_meta($user_id, "gender", true);
            $c['roles']			           		= $suser->roles;
        }
        else 
		{
			$suser = new StdClass();
			$suser->ID 				= -1;
			$suser->display_name 	= __("Unlogged User", BIO);			
		}
        if(is_wp_error($suser) || !$suser)
            return $c->get_error_message();
		else 
        return $c;
    }


	static function is_user_role( $role, $user_id = null ) 
	{
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		if( ! $user )
			return false;
		return in_array( $role, (array) $user->roles );
	}
	static function is_user_roles( $array_roles, $user_id = null ) 
	{
		if(!is_array($array_roles)) $array_roles = [$array_roles];
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		if( ! $user )
			return false;
		return array_intersect( $array_roles, (array) $user->roles );
	}
	static function get_roles( $user_id = null )
	{
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		$roles = array();
		if($user && is_array($user->roles))
		{
			foreach($user->roles as $role)
				$roles[] = $role;
		}
		return $roles;
	}
	static function is_subscribe_course( $course_id, $user_id = null )
	{
		global $wpdb;
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		$query = "SELECT course_id FROM " . $wpdb->prefix . "course_user WHERE user_id={$user->ID} AND course_id = $course_id;";
		return $wpdb->get_var($query);
	}
	static function is_subscribe_courses( $course_ids, $user_id = null )
	{
		global $wpdb, $rests_hook;
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		$query = "SELECT course_id FROM " . $wpdb->prefix . "course_user WHERE user_id={$user->ID} AND course_id IN(". implode(",", (Array)$course_ids) .");";
		$res 		= $wpdb->get_results($query);
		$cids 		= [];
		//$rests_hook = $query;
		foreach($res as $bo)
		{
			$cids[] = $bo->course_id;
		}
		return $cids;
	}
	static function is_member_course( $course_id, $user_id = null )
	{
		global $wpdb;
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		$query = "SELECT course_id FROM " . $wpdb->prefix . "course_user WHERE user_id=$user->ID AND course_id=$course_id;";
		return $wpdb->get_var($query);
	}
	
	static function get_favorites($user_id)
	{
		global $wpdb;
		$query = "SELECT post_id FROM `" . $wpdb->prefix . "user_article` WHERE user_id='$user_id';";
		return $wpdb->get_results($query);
	}

    static function get_favorites_count($user_id)
    {
        global $wpdb;
        $query = "SELECT COUNT(post_id) FROM `" . $wpdb->prefix . "user_article` WHERE user_id='$user_id';";
        return $wpdb->get_var($query);
    }
	/*
		$table: course_user 			- member
				course_user_requests 	- only request
	*/
	static function get_courses($user_id, $table="course_user")
	{
		global $wpdb;
		$query = "SELECT t.term_id, t.name, t.slug, GROUP_CONCAT(DISTINCT 'description', '##', tt.description, '---', tm.meta_key, '##', tm.meta_value SEPARATOR '---') AS meta, cu.date AS date
FROM `" . $wpdb->prefix . "$table` AS cu 
LEFT JOIN " . $wpdb->prefix . "terms as t ON t.term_id=cu.course_id 
LEFT JOiN " . $wpdb->prefix . "termmeta AS tm ON tm.term_id=cu.course_id 
LEFT JOIN " . $wpdb->prefix . "term_taxonomy AS tt ON tt.term_id=cu.course_id 
WHERE user_id='$user_id'
GROUP BY cu.course_id";
		$rer = $wpdb->get_results($query);
		$re = [];
		foreach($rer as $rr)
		{	
			$r = [];
			foreach($rr AS $key=>$val)
			{
				switch($key)
				{
					case "meta":
						$meta = explode("---", $val);
						foreach($meta as $m)
						{
							$mm = explode("##", $m);
							$r[$mm[0]] = $mm[1];
						}
						break;
					default:
						$r[$key] = $val;
						break;
				}
					
			}			
			$re[] = $r;
		}
		return $re;
	}
	static function get_my_courses_count($user_id)
	{		
        global $wpdb;
        $query = "SELECT COUNT(course_id) FROM " . $wpdb->prefix . "course_user WHERE user_id='$user_id';";
        return $wpdb->get_var($query);
	}

    static function get_courses_count($user_id, $table="course_user")
    {
        global $wpdb;
        $query = "SELECT t.term_id, t.name, t.slug, GROUP_CONCAT(DISTINCT 'description', '##', tt.description, '---', tm.meta_key, '##', tm.meta_value SEPARATOR '---') AS meta, cu.date AS date
FROM `" . $wpdb->prefix . "$table` AS cu 
LEFT JOIN " . $wpdb->prefix . "terms as t ON t.term_id=cu.course_id 
LEFT JOiN " . $wpdb->prefix . "termmeta AS tm ON tm.term_id=cu.course_id 
LEFT JOIN " . $wpdb->prefix . "term_taxonomy AS tt ON tt.term_id=cu.course_id 
WHERE user_id='$user_id'
GROUP BY cu.course_id";
        return $query;//$wpdb->get_var($query);
    }

	static function register_anonim($data)
	{
		global $wpdb, $rest_log1;
		$rest_log1 = $data;
		if( email_exists( $data['email'] ) )
		{
			return [ "message" => Bio_Messages::send_email_exists( $data['email'] )];
		}
		if( !is_email( $data['email'] ) )
		{
			return [ "message" => Bio_Messages::send_no_email( $data['email'] )];
		}
		$user_id = register_new_user(
			sanitize_file_name(sanitize_text_field($data['email'])), 
			sanitize_email($data['email'])
		);
		$userr = sanitize_text_field($data['user']);
		$names	= explode(" ", $userr);
		wp_update_user([
			"ID"			=> $user_id,
			"display_name"	=> $userr,
			"first_name"	=> $names[0],
			"last_name"		=> $names[1],
		]);
		$nuser = get_user_by("id", $user_id);
		$nuser->add_role("HiddenUser");
		if(class_exists("Bio_REST"))
		{
			Bio_REST::set_session( $nuser );
		}
		return [ "message" => Bio_Messages::register_anonim(), "user" => $nuser ];
	}
	static function get_users_per_courses( $user_id )
	{
		global $wpdb;
		$query  = "SELECT cur.course_id, GROUP_CONCAT(DISTINCT user_id) AS users 
		FROM `" . $wpdb->prefix . "course_user_requests` AS cur 
		LEFT JOIN " . $wpdb->prefix . "termmeta AS ptm ON ptm.term_id=cur.course_id 
		WHERE ptm.meta_key='author' AND ptm.meta_value=$user_id 
		GROUP BY course_id";
		$res = $wpdb->get_results($query);
		$r = [];
		foreach($res as $rr)
		{
			$r[ $rr->course_id ] = [ "users" => explode(",", $rr->users) ];
		}
		return $r;
	}
	static function find_user_email($search="34567890")
	{
		global $wpdb;
		$query = "SELECT ID, user_email, display_name FROM " . $wpdb->prefix . "users WHERE user_email LIKE '%$search%';";
		return $wpdb->get_results($query);
	}
	static function get_user($user_id)
	{
		$user = get_user_by("id", $user_id);
		// wp_die( [$user->ID, get_user_meta($user->ID, 'user_descr', true)] );
		return apply_filters(
			"bio_get_user",
			[
				"id"				=> $user->ID,
				"ID"				=> $user->ID,
				"display_name"		=> $user->display_name,
				"first_name"		=> $user->first_name,
				"user_descr"		=> get_user_meta($user->ID, 'user_descr', true),//$user->user_descr,
				"last_name"			=> $user->last_name,
				"roles"				=> $user->roles,
				"caps"				=> static::get_caps( $user_id ),
				"caps1"				=> decbin(static::get_caps( $user_id )),			
				"user_email"		=> $user->user_email,
				"account_activated"	=> get_user_meta($user->ID, 'account_activated', true),
				"is_blocked"		=> get_user_meta($user->ID, 'is_blocked', true),
				"__typename" 		=> "User"
			],
			$user
		);
	}
}
