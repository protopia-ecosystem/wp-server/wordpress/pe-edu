<?php
class Bio_Test_Category extends SMC_Taxonomy
{
	static function get_type()
	{
		return BIO_TEST_CATEGORY_TYPE;
	}
	static function init()
	{
		add_action( 'init', 				array( __CLASS__, 'create_taxonomy'), 19);
		add_action( 'parent_file',			array( __CLASS__, 'tax_menu_correction'), 1);	
		add_action( 'admin_menu', 			array( __CLASS__, 'tax_add_admin_menus'), 20);
		//add_filter("manage_edit-".BIO_TEST_CATEGORY_TYPE."_columns", 	array( __CLASS__,'ctg_columns')); 
		//add_filter("manage_".BIO_TEST_CATEGORY_TYPE."_custom_column",	array( __CLASS__,'manage_ctg_columns'), 11.234, 3);
		//add_action( BIO_TEST_CATEGORY_TYPE.'_edit_form_fields', 		array( __CLASS__, 'add_ctg'), 2, 2 );
		//add_action( 'edit_'.BIO_TEST_CATEGORY_TYPE, 					array( __CLASS__, 'save_ctg'), 10);  
		//add_action( 'create_'.BIO_TEST_CATEGORY_TYPE, 					array( __CLASS__, 'save_ctg'), 10);	
	}
	static function create_taxonomy()
	{
		register_taxonomy(
			static::get_type(), 
			array( BIO_TEST_TYPE ), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Test Category", BIO),
					'singular_name'     => __("Test Category", BIO),
					'search_items'      => __('search Test Category', BIO),
					'all_items'         => __('all Test Categories', BIO),
					'view_item '        => __('view Test Category', BIO),
					'parent_item'       => __('parent Test Category', BIO),
					'parent_item_colon' => __('parent Test Category:', BIO),
					'edit_item'         => __('edit Test Category', BIO),
					'update_item'       => __('update Test Category', BIO),
					'add_new_item'      => __('add Test Category', BIO),
					'new_item_name'     => __('new Test Category Name', BIO),
					'menu_name'         => __('Test Category', BIO),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_in_nav_menus' 	=> true,
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => true,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
		//wp_nav_menu_taxonomy_meta_boxes() ;
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pe_edu_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pe_edu_page', 
			__("Test Categories", BIO), 
			__("Test Categories", BIO), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
		add_meta_box( "add-".BIO_TEST_CATEGORY_TYPE."", __("Test Categories", BIO), 'wp_nav_menu_item_taxonomy_meta_box', 'nav-menus', 'side', 'default', static::get_type() );	
    }
	
	static function ctg_columns($theme_columns) 
	{
		$new_columns = array
		(
			'cb' 				=> ' ',
			//'id' 				=> 'id',
			'name' 				=> __('Name'),
			'icon' 				=> __('Icon', BIO),
			'is_international' 	=> __('Is international', BIO),
		);
		return $new_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		switch ($column_name) {
			case 'id':
				$out 		.= $term_id;
				break;
			case 'is_international': 
				$color = get_term_meta( $term_id, 'is_international', true ); 
				$out 		.=  $color ? "<img src='" . BIO_URLPATH . "assets/img/check_checked.png'> <span class='smc-label-782px'>" . $obj[$column_name]['name'] . "</span>" 
									: "<img src='" . BIO_URLPATH . "assets/img/check_unchecked.png'> <span class='smc-label-782px'>" . $obj[$column_name]['name'] . "</span>";
				break;	 
			case 'color': 
				$color = get_term_meta( $term_id, 'color', true ); 
				$out 		.= "<div class='clr' style='background-color:$color;'></div>";
				break;	 
			case "icon":
				$icon = get_term_meta( $term_id, 'icon', true ); 
				$logo = wp_get_attachment_image_src($icon, "full")[0];
				echo "<img src='$logo' style='width:auto; height:60px; margin:10px;' />";
				break;	
			default:
				break;
		}
		return $out;    
	}
	
	static function add_ctg( $term, $tax_name )
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		if($term)
		{
			$term_id = $term->term_id;
			$color = get_term_meta($term_id, "color", true);
			$icon  = get_term_meta($term_id, "icon", true);
			$is_international  = get_term_meta($term_id, "is_international", true);
			$icon  = is_wp_error($icon) ? "" :  $icon;
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="is_international">
					<?php echo __("Is international", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="checkbox" name="is_international" value="1"  <?php checked(1, $is_international, 1); ?>/>
			</td>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="icon">
					<?php echo __("Icon", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					echo get_input_file_form2( "group_icon", $icon, "group_icon", 0 );
				?>
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "color", $_POST['color']);
		update_term_meta($term_id, "icon",  $_POST['group_icon0']);
		update_term_meta($term_id, "is_international",  $_POST['is_international']);
	}
	
	static function update( $data, $post_id )
	{
		$post_id = (int)$post_id;
		wp_update_term( $post_id, static::get_type(), array(
			'name' 			=> $data["name"],
			'description' 	=> $data["description"],
		));
		update_term_meta($post_id, "icon", $data["icon"]);
		update_term_meta($post_id, "is_international", $data["is_international"]);
		return $post_id;
	}
	static function insert( $data )
	{
		$post_id = wp_insert_term( $data["name"], static::get_type(), array(
			'description' => $data["description"]
		) );
		update_term_meta($post_id, "icon", $data["icon"]);
		update_term_meta($post_id, "is_international", $data["is_international"]);
		return $post_id;
	}
	
	
	static function get_all()
	{
		$terms = get_terms( array(
			'taxonomy'      => static::get_type(), 
			'orderby'       => 'name', 
			'order'         => 'ASC',
			'hide_empty'    => false, 
			'fields'        => 'all', 
		) );
		return $terms;
	}
	static function wp_dropdown($params=-1)
	{
		if(!is_array($params))
			$params = [];
		if($params['terms'])
		{
			$terms		=  $params['terms'];
		}
		else
		{
			$terms = get_terms( array(
				'taxonomy'      => static::get_type(), 
				'orderby'       => 'name', 
				'order'         => 'ASC',
				'hide_empty'    => false, 
				'fields'        => 'all', 
			) );
		}
		$html		= "<select ";
		if($params['class'])
			$html	.= "class='".$params['class']."' ";
		if($params['style'])
			$html	.= "style='".$params['style']."' ";
		if($params['name'])
			$html	.= "name='".$params['name']."' ";
		if($params['id'])
			$html	.= "id='".$params['id']."' ";
		if($params['special'])
		{
			$pars  	= explode(",", $params['special']);
			$html	.= "$pars[0]='$pars[1]' ";
		}
		$html		.= " >";
		$zero 		= $params['select_none'] ? $params['select_none'] : "---";
			if(!$params['none_zero'])
				$html	.= "<option value='-1' selected>$zero</option>";			
			
		if(count($terms))
		{
			foreach($terms as $term)
			{
				$html	.= "
				<option " . selected($term->term_id, $params['selected'], 0) . " value='".$term->term_id."'>".
					$term->name.
				"</option>";
			}
		}
		$html		.= apply_filters("bio_olimpiad_type_last_dropdown", "", $params, $terms) . "
		</select>";
		return $html;
	}
	
    public static function api_action($type, $methods, $code, $pars, $user)
    {
        $courses	= [];
        switch($methods) {
            case "update":
                if(is_numeric($code)) 
				{
                    //Bio_Olimpiad_Type::update($pars, $code);
                    $articles[]	= [];
					$cat = static::get_category( $code );
					$msg = sprintf( __("Test Category «%s» updated succesfully", BIO), $cat['post_title'] ); 
					$courses[]	= $cat;
                }
				else
				{
                    $msg = __("Test Category inserted succesfully", BIO);
                }
                break;
            case "delete":
                if(is_numeric($code)) {
                    Bio_Olimpiad_Type::delete($code);
                    $update = 'success';
                }else{
                    $update = 'error';
                }
                break;
            case "create":
                if(is_numeric($code)) 
				{
                    Bio_Olimpiad_Type::update($pars, $code);
                    $articles[]	= [];
					$courses[]	= static::get_category( $code );
					$msg = __("Test Category updated succesfully---", BIO);
                }
				else
				{
                    $class = Bio_Olimpiad_Type::insert($pars);
                    $articles[] = static::get_category($class);
					$courses[]	= static::get_category( $code );
                    $msg = __("TestCategory inserted succesfully", BIO);
                }
                break;
            case "read":
            default:
                //$code = (int)$code;
                if(is_numeric($code))
				{
                    $articles = [];
                    $all 	= Bio_Test::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: [], 		// []
                        isset($pars['numberposts'])		? $pars['numberposts'] 	: -1,  		// -1
                        isset($pars['offset'])			? $pars['offset']		: 0,  		// 0
                        isset($pars['order_by'])		? $pars['order_by']		: "post_date", 	// 'title'
                        isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
                        isset($pars['order_by_meta'])	? $pars['order_by_meta']: "", 		// ""
                        "all", 																// $pars['fields'],
                        isset($pars['relation'])		? $pars['relation']		: "AND",	// "AND",
                        isset($pars['author'])			? $pars['author']		: -1,		// "",
                        [ "bio_test_category" => $code ]

                    );
                    foreach($all as $p)
                    {
                        $a 					= Bio_Test::get_test( $p, false );
                        $articles[]			= $a;
                    }
                    $c					= static::get_category( $code );
                    $c['count']			= count(Bio_Test::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: [], 		// []
                        -1,  		// -1
                        0,  		// 0
                        isset($pars['order_by'])		? $pars['order_by']		: "post_date", 	// 'title'
                        isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
                        isset($pars['order_by_meta'])	? $pars['order_by_meta']: "", 		// ""
                        "ids", 																// $pars['fields'],
                        isset($pars['relation'])		? $pars['relation']		: "AND",	// "AND",
                        isset($pars['author'])			? $pars['author']		: -1,		// "",
                        [ "bio_test_category" => $code ]

                    ));
                    $courses[] 			= $c;
                }
				else
				{
                    $terms = get_terms( array(
                        'taxonomy'      => static::get_type(),
                        'orderby'       => 'name',
                        'order'         => 'ASC',
                        'hide_empty'    => false,
                        'object_ids'    => null,
                        'include'       => array(),
                        'exclude'       => array(),
                        'exclude_tree'  => array(),
                        'number'        => '',
                        'fields'        => 'all',
                        'count'         => false,
                        'slug'          => '',
                        'parent'         => '',
                        'hierarchical'  => true,
                        'child_of'      => 0,
                        'offset'        => '',
                        'name'          => '',
                        'childless'     => false,
                        'update_term_meta_cache' => true,
                        'meta_query'    => '',
                    ) );
                    foreach($terms as $c)
                    {
                        $courses[]	= static::get_category( $c );
                    }
                }
                break;
        }

        return [
            "bio_test_category" => $courses,
            "tests" => $tests,
            "id" => $code,
            "msg" => $msg,
            "update"=> $methods
        ];
    }
	
    public static function get_category($p)
    {
        if(is_numeric($p))
        {
            $course = get_term($p, static::get_type());
        }
        else
        {
            $course = $p;
        }
        $c = [];
        if(is_wp_error($course) || !$course)
            return $c;
        $c['id']					= $course->term_id;
        $c['post_title']			= $course->name;
        $c['post_content']			= $course->description;
        return $c;
    }

}
	