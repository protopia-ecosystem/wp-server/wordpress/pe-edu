<?php
class Bio_Page extends SMC_Post
{
    static function get_type()
    {
        return "Page";
    }


    static function get_page( $p )
    {
        $art				= is_numeric($p) ? get_post($p) : $p;
        $a					= [];
        $a['id']			= $art->ID;
        $a['post_title']	= $art->post_title;
        $a['post_content']	= $art->post_content;
        $a['post_date']		= date_i18n( 'j F Y', strtotime( $art->post_date ) );
        $thumbnail			= get_the_post_thumbnail_url( $art->ID, "full" );
        $a['thumbnail']		= $thumbnail ? $thumbnail : BIO_EMPTY_IMG;

        // Categories

        $cats				= [];
        $categs	= get_the_terms($art->ID, "category");
        if(count($categs) && $categs && !is_wp_error($categs))
        {
            foreach($categs as $categ)
            {
                $cats[]			= static::get_category( $categ );
            }
        }
        $a["category"]		= $cats;
        /**/
        // author
        $user				= get_user_by("id", $art->post_author);
        $auth				= [];
        $auth["id"]			= $user->ID;
        $auth["display_name"]= $user->display_name;
        $a['post_author']	= $auth;

        return $a;
    }

    public static function api_action($type, $methods, $code, $pars, $user)
    {
        $pages	= [];
        switch($methods) {
            case "update":
				//if( ! Bio_User::is_user_cap(BIO_OLIMPIAD_TYPE_CREATE) )
				//	throw new ExceptionNotAccessREST(BIO_ACCESS_ERROR_MESSAGE);
                if(is_numeric($code)) {
					$content 		= Bio_Assistants::extract_img( $pars['post_content'], "post_image", $code );
                    wp_update_post(["ID" => $code,
                        "post_title" => $pars['post_title'],
                        "post_name" => $pars['post_title'],
                        "post_content" => $content
                    ]);
                    $pages[]	= get_post($code);
                    $msg = 'success';
                }else{
                    $msg = 'error';
                }
                break;
            case "delete":
                if(is_numeric($code)) {
                    wp_delete_post($code);
                    $msg = __("Page removed succesfully", BIO);
                }else{
                    $msg = 'error';
                }
                break;
            case "create":
                if(is_numeric($code)) 
				{
					Bio_User::access_caps(BIO_OLIMPIAD_TYPE_CREATE, "Update post");
					$content 		= Bio_Assistants::extract_img( $pars['post_content'], "post_image", $code );
                    wp_update_post(["ID" => $code,
                        "post_title" => $pars['post_title'],
                        "post_name" => $pars['post_title'],
                        "post_content" => $content
                    ]);

                    if( $pars['thumbnail_id'] < 1 )
                    {
                        $media = Bio_Assistants::insert_media([
                            "data" => $pars['thumbnail'],
                            "media_name"=> $pars['media_name']
                        ], $code);
                        $pars['thumbnail_id']	= $media['id'];
                        $pars['thumbnail']		= $media['url'];
						wp_set_object_terms( $media['id'], (int)Bio::$options['test_media_free'], BIO_MEDIA_TAXONOMY_TYPE );
                    }

                    if( $pars["thumbnail_id"] > 0 )
                    {
                        set_post_thumbnail(
                            $code,
                            (int) $pars["thumbnail_id"]
                        );
                    }

                    $p	= get_post($code);
                    $msg = sprintf( __("Page «%s» updated succesfully", BIO), $p->post_title ); 
					$pages[] = $p;
                }
				else
				{
                    $ID =  wp_insert_post([
                        "post_title" => $pars['post_title'],
                        "post_name" => $pars['post_title'],
                        "post_content" => $pars['post_content'],
                        "post_status" => "publish",
                        "post_type" => "page"
                    ]);

                    if( $pars['thumbnail_id'] < 1 )
                    {
                        $media = Bio_Assistants::insert_media([
                            "data" => $pars['thumbnail'],
                            "media_name"=> $pars['media_name']
                        ], $ID);
						wp_set_object_terms( $media['id'], (int)Bio::$options['test_media_free'], BIO_MEDIA_TAXONOMY_TYPE );
                        $pars['thumbnail_id']	= $media['id'];
                        $pars['thumbnail']		= $media['url'];
                    }

                    if( $pars["thumbnail_id"] > 0 )
                    {
                        set_post_thumbnail(
                            $ID,
                            (int) $pars["thumbnail_id"]
                        );
                    }

                    $pages[]	= get_post($ID);
                    $msg = __("Page inserted succesfully", BIO);
                }
                break;
            case "read":
            default:
                if(is_numeric($code))
				{
                    $pg					= static::get_page( $code );
					//$pg['imgs'] 		= static::extract_img( $pg['post_content'], $pg['post_title'] );
					$pages[] 			= $pg;
                }
				else
				{
                    $ps = get_posts([
                        "post_type"	=>"page",
                        "post_status"	=> "publish",
                        "numberposts" => -1
                    ]);

                    foreach($ps as $pp)
                    {
                        $pg			= static::get_page( $pp );
						//$pg['imgs']	= static::extract_img( $pg['post_content'], $pg['post_title'] );
						$pages[] 	= $pg;
						
                    }

                }
                break;
        }

        return Array (
            "pages" => $pages,
            "id" => $code,
            "msg" => $msg
        );

    }
	


}