<?php

class Bio_Test_API
{
	public static $memcached_instance;
	
	public static function memcached()
	{
		if (!self::$memcached_instance)
		{
			self::$memcached_instance = new Memcache;
			self::$memcached_instance->connect('127.0.0.1', 11211);
		}
		return self::$memcached_instance;
	}
	
	public static function test_refresh($test_id)
	{
		self::memcached()->delete("test|" . $test_id . "|" . "public" . "|" . "with_hidden");
		self::memcached()->delete("test|" . $test_id . "|" . "private" . "|" . "with_hidden");
		self::memcached()->delete("test|" . $test_id . "|" . "public" . "|" . "without_hidden");
		self::memcached()->delete("test|" . $test_id . "|" . "private" . "|" . "without_hidden");

		if (Bio_Test::is_test_export_xml($test_id))
		{
			self::export_xml($test_id, ABSPATH . "export/test_{$test_id}.zip");
			self::list_xml(ABSPATH . "export", ABSPATH . "export/cmp_quizes.xml", "https://{$_SERVER["SERVER_NAME"]}/export/");
		}
	}
	
	public static function test_get($test_id, $public = true, $with_hidden = false)
	{
		global $rest_log;
		$rest_log =[];
		/*
			(bool)				get_post_meta($test_id, "is_timed", true);
			(int)				get_post_meta($test_id, "duration", true);
			(bool)				get_post_meta($test_id, "is_show_results", true);
			(bool)				get_post_meta($test_id, "is_shuffle", true);
			(int)				get_post_meta($test_id, "group", true);
			([term_taxonomy])	get_the_terms($test_id, BIO_CLASS_TYPE);
			([term_taxonomy])	get_the_terms($test_id, BIO_TEST_CATEGORY_TYPE);
			([term_taxonomy])	get_the_terms($test_id, BIO_BIOLOGY_THEME_TYPE);
		*/
		
		$memcached_key = "test|" . $test_id . "|" . ($public ? "public" : "private") . "|" . ($with_hidden ? "with_hidden" : "without_hidden");
		$result = self::memcached()->get($memcached_key);
		if (!$result)
		{
			global $wpdb;
			$where = $with_hidden ? "" : "AND hidden = 0";
			$questions_raw = $wpdb->get_results("
				SELECT q.*, pm.meta_value AS image_url
				FROM bio_test_question tq
				LEFT JOIN bio_question q ON tq.question_id = q.id
				LEFT JOIN {$wpdb->prefix}postmeta AS pm ON post_id=q.image_id
				WHERE tq.test_id = {$test_id} AND q.is_deleted = 0 {$where}
				ORDER BY position
			", ARRAY_A);
			$questions = [];
			foreach ($questions_raw as $question)
			{
				$questions[$question["id"]] = $question;
				$questions[$question["id"]]["answers"] = [];
			}
				$rest_log[] = $questions;
			/*
			if (get_post_meta($test_id, "is_shuffle", true))
			{
				//shuffle($questions);
			}
			*/
			$query = "
				SELECT a.*
				FROM bio_answer a
				LEFT JOIN bio_question q ON a.question_id = q.id
				LEFT JOIN bio_test_question tq ON tq.question_id = q.id
				WHERE tq.test_id = {$test_id} AND a.is_deleted = 0
				ORDER BY a.position
			";
			$answers_raw = $wpdb->get_results($query, ARRAY_A);
			$answers = [];
			foreach ($answers_raw as $answer)
			{
				$answer['single'] = $answer['single'] == "false" || $answer['single'] == "0" || $answer['single'] == ""
					?
					false
					:
					true;
				$answers[$answer["id"]] = $answer;
			}
			
			foreach ($answers as $answer)
			{
				if ($public)
				{
					unset($answer["fraction"]);
					unset($answer["subquestion_answer_id"]);
				}
				$questions[$answer["question_id"]]["answers"][] = $answer;
				$rest_log[] = $questions[$answer["question_id"]];
			}
			$question["answers"] = $answers;
			
			if ($public)
			{
				foreach ($questions as &$question)
				{
					if ($question["shuffleanswers"])
					{
						shuffle($question["answers"]);
					}
					/*if ($question["type"] == "matching")
					{
						$texts = [];
						$subquestion_texts = [];
						foreach ($question["answers"] as $k => $answer)
						{
							$texts[$k] = $answer["text"];
						}
						shuffle($texts);
						foreach ($question["answers"] as $k => &$answer)
						{
							$answer = $texts[$k];
						}
					}*/
				}
			}
			
			foreach ($questions as &$question)
			{
				$question["answers"] = array_values($question["answers"]);
			}
			$questions = array_values($questions);
			
			$result = ["questions" => $questions];
			self::memcached()->set($memcached_key, $result);
		}
		
		return $result;
	}
	
	public static function get_single_question_right($question_id)
	{
		global $rest_log, $wpdb;
		$query = "
			SELECT a.*
			FROM bio_answer a 
			WHERE a.question_id = {$question_id} AND a.is_deleted = 0 AND fraction > 0
			ORDER BY a.position
		";
		return $wpdb->get_results($query, ARRAY_A);
	}
	
	public static function export_xml($test_id, $filename)
	{
		// return;
		if(!class_exists("SimpleXMLElement")) return;
		$test = self::test_get($test_id, false, true);
		
		$test_sxml = new SimpleXMLElement("<quiz></quiz>");
		
		if(!method_exists($test_sxml,'addChild')) return;
				
		$name_sxml = $test_sxml->addChild("question");
		$name_sxml->addAttribute("type", "category");
		$name_sxml->addChild("category")->addChild("text", '$module$/' . Bio_Test::get_test_name($test_id));
		
		foreach ($test["questions"] as $question)
		{
			$question_sxml = $test_sxml->addChild("question");
			$question_sxml->addAttribute("type", $question["type"]);
			$question_sxml->addChild("questiontext", $question["questiontext"]);
			$question_sxml->addChild("single", $question["single"]);
			$question_sxml->addChild("name")->addChild("text", $question["name"]);
			foreach ($question["answers"] as $answer)
			{
				$answer_sxml = $answer_sxml->addChild("answer");
				$answer_sxml->setAttribute("fraction", $answer["fraction"]);
				$answer_sxml->addChild("text", $answer["text"]);
			}
		}
		$zip = new ZipArchive();
		$zip->open($filename, ZipArchive::CREATE);
		$zip->addFromString("test_{$test_id}.xml", $test_sxml->asXML());
		$zip->close();
	}
	
	public static function list_xml($path, $filename, $url)
	{
		return;
		if(!class_exists("SimpleXMLElement")) return;
		$list_sxml = new SimpleXMLElement("<quiz></quiz>");
		$files = scandir($path);
		foreach ($files as $file)
		{
			if (preg_match("/^test_([0-9]+).zip/", $file, $matches))
			{
				$test_sxml = simplexml_load_file($path . "/" . $file);
				$name = $test_sxml->xpath("/quiz[@type='category']/category/text")[0];
				$name = explode("/", $name);
				$name = $name[1];
				
				if(method_exists($list_sxml,'addChild'))
				{
					$quiz_sxml = $list_sxml->addChild("quiz");
					$quiz_sxml->addAttribute("id", $matches[1]);
					$quiz_sxml->addAttribute("name", $name);
					$quiz_sxml->addAttribute("url", $url . $matches[1] . ".zip");
				}
			}
		}
		file_put_contents($filename, $list_sxml->asXML());
	}
	
	public static function test_add($test_id, $questions)
	{
		$test_id = intval($test_id);
		global $wpdb;
		
		$position=0;
		foreach($questions as $question_id)
		{
			$position++;
			self::question_add_to_test($question_id, $test_id);
			$wpdb->query("UPDATE bio_test_question SET position = {$position} WHERE question_id = {$question_id} AND test_id = {$test_id}");
		}
		
		self::test_refresh($test_id);
		return true;
	}
	
	public static function test_edit( $test_id, $questions_add, $questions_edit, $questions_delete, $questions )
	{
		$test_id = intval($test_id);
		global $wpdb;
		$position=0;
		static::test_question_clear($test_id);
		foreach($questions as $question_id)
		{
			self::question_add_to_test($question_id, $test_id, $position);
			$position++;
		}
		/*
		foreach($questions_add as $question_id)
		{
			self::question_add_to_test($question_id, $test_id);
		}
		foreach($questions_delete as $question_id)
		{
			self::question_remove_from_test($question_id, $test_id);
		}
		*/
		self::test_refresh($test_id);
		return true;
	}
	
	public static function test_delete($test_id)
	{
		$test_id = intval($test_id);
		global $wpdb;
		
		self::test_refresh($test_id);
		return true;
	}
	
	public static function get_question($question_id)
	{
		global $wpdb, $rest_log;
		$query = "SELECT q.id, q.type, q.answernumbering , q.image_id, q.hidden, q.questiontext, q.penalty, q.shuffleanswers, q.single, q.position, q.author_id, q.bio_biology_theme_id,
		pt.name
		FROM `bio_question` q 
		LEFT JOIN {$wpdb->prefix}terms pt ON pt.term_id=q.bio_biology_theme_id
		WHERE ID={$question_id}";
		$rest_log = $query;
		return $wpdb->get_results($query);
	}
	
	public static function get_questions($params=[])
	{
		global $wpdb, $rest_log;
		$where_arr 	= [];
		$_fields 	= [];
		//$fields		= $params['fields'] ? $params['fields'] : [];
		if( $params['test_id'] && (int)$params['test_id'] > 0 )
		{
			$where_arr[] = "bt.test_id=".$params['test_id'];
			
		}
		if( $params['bio_biology_theme'] && (int)$params['bio_biology_theme'] > 0 )
		{
			$where_arr[] = "q.bio_biology_theme_id=".$params['bio_biology_theme'];
			
		}
		if( isset($params['single']) )
		{
			if($params['single'])
			{
				$where_arr[] = " ( q.single='true' OR q.single='1' ) ";
			}
			else
			{
				$where_arr[] = " (q.single='false' OR q.single='0' OR q.single='' ) ";
			}
			
		}
		if( isset($params['hidden']) )
		{
			if($params['hidden'])
			{
				$where_arr[] = " ( q.hidden='true' OR q.hidden='1' ) ";
			}
			else
			{
				$where_arr[] = " (q.hidden='false' OR q.hidden='0' OR q.hidden='' ) ";
			}
			
		}
		if( $params['type'] )
		{
			$where_arr[] = "q.type='".$params['type']."'";
			
		}
		if( $params['search'] )
		{
			$where_arr[] = "q.name LIKE '%".$params['search']."%'";
			
		}
		if( (int)$params['post_author'] )
		{
			$where_arr[] = "q.author_id=".$params['post_author'];
			
		}
		if((int)$params['is_errors'])
		{
			$where_arr[] = " q.id IN(SELECT DISTINCT question_id FROM ". $wpdb->prefix. "error_test) ";
		}
		if(count($where_arr))
		{
			$where = "WHERE " . implode(" AND ", $where_arr);
		}
		if(count($fields))
		{
			$fields = "," . implode(", ", $_fields);
		}
		$result_fields = $params['result_fields']  
			? 
			$params['result_fields'] 
			:
			"q.*, 
			". $wpdb->prefix. "posts.post_title AS test_title, 
			et.ID AS error_id, 
			et.comment, 
			et.date AS comment_date, 
			users.display_name AS display_name, 
			users.user_email,
			pt.term_id AS bio_biolody_theme 	
			$fields";

		$query = "SELECT $result_fields
		FROM bio_question q
		LEFT JOIN bio_test_question bt ON bt.question_id = q.id
		LEFT JOIN por_posts ON ". $wpdb->prefix. "posts.ID=bt.test_id  
		LEFT JOIN ". $wpdb->prefix. "error_test et ON et.question_id=q.id
		LEFT JOIN ". $wpdb->prefix. "terms pt ON pt.term_id=q.bio_biology_theme_id
		LEFT JOIN ". $wpdb->prefix. "users users ON users.ID=et.user_id
		$left_join
		$where
		ORDER BY questiontext
		LIMIT " . (int)$params['offset'] * (int)$params['numberposts'] . "," . (int)$params['numberposts'] . ";";
		
		// wp_die( join(" ", explode("\t", join(" ", explode("\n", $query)))) );
		
		$rest_log = $query; 
		// wp_die( $wpdb->get_results($query));
		return $wpdb->get_results($query);
	}
	
	public static function question_add($test_id, $type, $position, $image_id, $name, $questiontext, $single, $shuffleanswers, $answers_add, $answers_edit, $answers_delete, $answers)
	{
		$test_id = intval($test_id);
		global $wpdb;
		
		$wpdb->insert("bio_question",
		[
			"test_id" => $test_id,
			"type" => $type,
			"position" => $position,
			"image_id" => $image_id,
			"name" => $name,
			"questiontext" => $questiontext,
			"single" => $single,
			"shuffleanswers" => $shuffleanswers,
		]
		);
		$question_id = $wpdb->insert_id;
		
		$i = 0;
		foreach($answer_add as $answer)
		{
			$i++;
			self::answer_add($question_id, $i, $answer["fraction"], $answer["text"], $answer["is_subquestion"], $answer["subquestion_answer_id"]);
		}

		self::test_refresh($test_id);
		return $question_id;
	}
	
	public static function question_edit($question_id, $type, $position, $image_id, $name, $questiontext, $single, $shuffleanswers, $answers_add, $answers_edit, $answers_delete, $answers)
	{
		global $wpdb;
		$question_id = intval($question_id);
		$test_id = $wpdb->get_var("SELECT test_id FROM question WHERE id = {$question_id}");
		
		$wpdb->update("bio_question",
		[
			"type" => $type,
			"position" => $position,
			"image_id" => $image_id,
			"name" => $name,
			"questiontext" => $questiontext,
			"single" => $single,
			"shuffleanswers" => $shuffleanswers,
		],
		["question_id" => $question_id]
		);
		
		foreach($answer_add as $answer)
		{
			self::answer_add($question_id, $answer["position"], $answer["fraction"], $answer["text"], $answer["is_subquestion"], $answer["subquestion_answer_id"]);
		}
		foreach($answer_edit as $answer)
		{
			self::answer_edit($answer["id"], $answer["position"], $answer["fraction"], $answer["text"], $answer["is_subquestion"], $answer["subquestion_answer_id"]);
		}
		foreach($answer_delete as $answer_id)
		{
			self::answer_delete($answer_id);
		}

		self::test_refresh($test_id);
	}
	
	public static function question_delete($question_id)
	{
		global $wpdb;
		$question_id = intval($question_id);
		$test_id = $wpdb->get_var("SELECT test_id FROM question WHERE id = {$question_id}");
		
		$wpdb->query("UPDATE bio_answer SET is_deleted = 1 WHERE question_id = {$question_id}");
		$wpdb->query("UPDATE bio_question SET is_deleted = 1 WHERE id = {$question_id}");
		
		self::test_refresh($test_id);
	}

	public static function question_verify($question_id)
	{
		global $wpdb;
		$question_id = intval($question_id);
		$test_id = $wpdb->get_var("SELECT test_id FROM bio_test_question WHERE question_id = {$question_id}");
		
		$wpdb->update("bio_question",
		[
			"hidden" => 0,
		],
		"question_id = {$question_id}"
		);
		
		self::test_refresh($test_id);
	}
	
	public static function question_add_to_test($question_id, $test_id, $position=0)
	{
		global $wpdb;
		$wpdb->query("
			INSERT INTO bio_test_question (test_id, question_id, position)
			VALUES({$test_id}, {$question_id}, {$position})
		", ARRAY_A);

		self::test_refresh($test_id);

	}
	
	public static function test_question_clear( $test_id )
	{		
		global $wpdb;
		$wpdb->query("
			DELETE FROM bio_test_question 
			WHERE test_id = {$test_id}");

		self::test_refresh($test_id);

	}
	
	public static function question_remove_from_test($question_id, $test_id)
	{
		global $wpdb;
		$wpdb->query("
			DELETE FROM bio_test_question 
			WHERE test_id = {$test_id} AND question_id = {$question_id})
		", ARRAY_A);

		self::test_refresh($test_id);

	}
	
	public static function answer_add($question_id, $position, $fraction, $text, $is_subquestion = false, $subquestion_answer_id = null)
	{
		global $wpdb;
		
		$question_id = intval($question_id);
		$test_id = $wpdb->get_var("SELECT test_id FROM question WHERE id = {$question_id}");
		$wpdb->insert("bio_answer",
		[
			"question_id" => $question_id,
			"position" => $position,
			"fraction" => $fraction,
			"text" => $text,
			"is_subquestion" => $is_subquestion,
			"subquestion_answer_id" => $subquestion_answer_id,
		]
		);
		
		self::test_refresh($test_id);
		return $wpdb->insert_id;
	}
	
	public static function answer_edit($answer_id, $position, $fraction, $text, $is_subquestion = false, $subquestion_answer_id = null)
	{
		global $wpdb;
		$answer_id = intval($answer_id);
		$test_id = $wpdb->get_var("
			SELECT q.test_id 
			FROM answer a
			LEFT JOIN question q ON q.id = a.question_id
			WHERE a.id = {$answer_id}
		");
		
		$wpdb->update("bio_answer",
		[
			"position" => $position,
			"fraction" => $fraction,
			"text" => $text,
			"is_subquestion" => $is_subquestion,
			"subquestion_answer_id" => $subquestion_answer_id,
		],
		["answer_id" => $answer_id]
		);
		
		self::test_refresh($test_id);
	}
	
	public static function answer_delete($answer_id)
	{
		global $wpdb;
		$answer_id = intval($answer_id);
		$test_id = $wpdb->get_var("
			SELECT q.test_id 
			FROM answer a
			LEFT JOIN question q ON q.id = a.question_id
			WHERE a.id = {$answer_id}
		");
			
		$wpdb->query("UPDATE bio_answer SET is_deleted = 1 WHERE id = {$answer_id}");
		
		self::test_refresh($test_id);
	}
	
	public static function get_result($result_id)
	{
		global $wpdb;
		
		$results = $wpdb->get_results("
				SELECT r.id, r.credits, r.right_count, r.test_id, r.user_id, u.login as user_login, u.display_name as user_display_name
				FROM bio_result r
				LEFT JOIN {$wpdb->prefix}users u ON u.ID = r.user_id
				WHERE r.id = {$result_id}
				ORDER BY r.start_time
			", ARRAY_A);
		if ($results)
		{
			$results = $results[0];
			$results["test"] = self::test_get($results["test_id"]);
			$results["questions"] = $wpdb->get_results("
				SELECT *
				FROM bio_result_question
				WHERE result_id = {$result_id}
			", ARRAY_A);
			foreach ($result["questions"] as &$question)
			{
				$question["answers"] = $wpdb->get_results("
					SELECT *
					FROM bio_result_answer
					WHERE result_question_id = {$question["id"]}
				", ARRAY_A);
			}
		}
		return $results;
	}
	public static function get_question_answers( $question_id )
	{
		global $wpdb;
		$x  = $wpdb->get_results("
			SELECT *
			FROM bio_answer
			WHERE question_id = {$question_id} 
		", ARRAY_A);
		// wp_die( $x );
		$answers = [];
		foreach($x as $xx)
		{
			$answers[] = [
				"id" 					=> $xx['id'],
				"post_content" 			=> $xx['text'],
				"fraction" 				=> $xx['fraction'],
				"is_deleted" 			=> $xx['is_deleted'],
				"is_subquestion"		=> $xx['is_subquestion'],
				"subquestion_answer_id"	=> $xx['subquestion_answer_id'],
				"position" 				=> $xx['position'],
			];
		}
		return $answers;
	}
	public static function get_results_of_test($test_id)
	{
		global $wpdb;
		
		$results = $wpdb->get_results("
				SELECT r.id, r.credits, r.right_count, r.test_id, r.user_id, u.login as user_login, u.display_name as user_display_name
				FROM bio_result r
				LEFT JOIN {$wpdb->prefix}users u ON u.ID = r.user_id
				WHERE r.test_id = {$test_id}
				ORDER BY r.start_time
			", ARRAY_A);
		if ($results)
		{
			$results["test"] = self::test_get($results["test_id"]);
		}

		return $results;		
	}
	
	public static function get_users_of_test($test_id)
	{
		global $wpdb;		
		$results = $wpdb->get_results("
				SELECT DISTINCT r.user_id, u.login as user_login, u.display_name as user_display_name
				FROM bio_result r
				LEFT JOIN {$wpdb->prefix}users u ON u.ID = r.user_id
				WHERE r.test_id = {$test_id}
				ORDER BY r.start_time
			", ARRAY_A);

		return $results;		
	}
	
	public static function get_results_of_user($user_id)
	{
		global $wpdb;		
		$results = $wpdb->get_results("
				SELECT p.post_title AS test_title, r.id, UNIX_TIMESTAMP(r.start_time) AS start_time, UNIX_TIMESTAMP(r.end_time) AS end_time, r.credits, r.right_count, r.test_id, r.user_id, u.user_login, u.display_name as user_display_name 
				FROM bio_result r
				LEFT JOIN {$wpdb->prefix}users u ON u.ID = r.user_id
				LEFT JOIN {$wpdb->prefix}posts p ON p.ID = r.test_id
				WHERE r.user_id = {$user_id}
				ORDER BY r.start_time
			", ARRAY_A);
		return $results;		
	}
	
	public static function get_count_results_of_user($test_id, $user_id)
	{
		global $wpdb;		
		return $wpdb->get_var("
			SELECT count(id)
			FROM bio_result
			WHERE user_id = {$user_id} AND test_id = {$test_id}
		");
	}
	
	/*
	result:
	questions => [
			[
				question_id => int,
				answered => bool,
				answers => [
					[
						answer_id => int,
						checked => bool,
						subquestion_answer_id => id
					]
				]
			]
		]
	*/
	public static function add_result($test_id, $user_id, $start_time, $end_time, $result)
	{
		global $wpdb;
		
		$test = self::test_get($test_id, false);
		$test_id = intval($test_id);
		$user_id = intval($user_id);
		
		$wpdb->insert("bio_result",
		[
			"test_id" => $test_id,
			"user_id" => $user_id,
			"start_time" => $start_time,
			"end_time" => $end_time,
		]
		);
		$result_id = $wpdb->insert_id;
		$credits = 0;
		$right_count = 0;
		$wrong_count = 0;
		$answered_count = 0;
		$questions_status = [];
		$questions_answers = [];
		
		foreach ($test["questions"] as $question)
		{
			$question_status = [
				"id" => $question["id"], 
				"questiontext" => $question["questiontext"],
				"type" => $question["type"],
				"single" => $question["single"],
			];
			
			if (!isset($result["questions"][$question["id"]]))
			{
				throw new Exception("Question result {$question["id"]} is not get");
			}
			$result_question = $result["questions"][$question["id"]];
			
			$wpdb->insert("bio_result_question",
			[
				"result_id" => $result_id,
				"question_id" => $result_question["question_id"],
				"answered" => $result_question["answered"],
			]
			);
			$result_question_id = $wpdb->insert_id;
			
			if ($result_question["answered"])
			{
				$questions_answers[$question["id"]]["answers"] = [];
				
				
				$answered_count++;
				$wrong_answer = false;
				foreach ($question["answers"] as $answer)
				{
					if (!isset($result["questions"][$question["id"]]["answers"][$answer["id"]]))
					{
						throw new Exception("Answer result {$answer["id"]} is not get.");
					}
					$result_answer = $result["questions"][$question["id"]]["answers"][$answer["id"]];
					
					if ($question["type"] != "matching" && $question["single"])
					{
						if ($answer["fraction"] && $result_answer["checked"])
						{
							$credits += 1;
						}
						elseif (!$answer["fraction"] && !$result_answer["checked"])
						{
						}
						else
						{
							$wrong_answer = true;
						}
					}
					elseif ($question["type"] != "matching" && !$question["single"])
					{
						if ($answer["fraction"] && $result_answer["checked"])
						{
							$credits += 1 * 2.5 / count($question["answers"]);
						}
						elseif (!$answer["fraction"] && !$result_answer["checked"])
						{
							$credits += 1 * 2.5 / count($question["answers"]);
						}
						else
						{
							$wrong_answer = true;
						}
					}
					else
					{
						if ($result_answer["subquestion_answer_id"] == $answer["subquestion_answer_id"])
						{
							$credits += 0.5;
						}
						else
						{
							$wrong_answer = true;
						}
					}
					
					$wpdb->insert("bio_result_answer",
					[
						"result_question_id" => $result_question_id,
						"answer_id" => $result_answer["answer_id"],
						"checked" => $result_answer["checked"],
						"subquestion_answer_id" => $result_answer["subquestion_answer_id"],
					]
					);			
					
					if (true || get_post_meta($test_id, "is_show_results", true))
					{
						$questions_answers[$question["id"]]["answers"][$answer["id"]] = 
							$answer["fraction"]  ? 1 : 0;
					}
				}

				if (!$wrong_answer)
				{
					$right_count++;
					$question_status["status"] = "right";
				}
				else
				{
					$wrong_count++;
					$question_status["status"] = "wrong";
				}
			}
			else
			{
				$question_status["status"] = "ignored";
			}
			
			$questions_status[] = $question_status;
		}
		$results = [
			"result_id" => $result_id,
			"all_count" => count($test["questions"]),
			"answered_count" => $answered_count,
			"right_count" => $right_count,
			"wrong_count" => $wrong_count,
			"questions_status" => $questions_status,
			"questions_answers" => $questions_answers,
			"credits" => $credits,
		];
		if (true || get_post_meta($test_id, "is_show_results", true))
		{
			//$results["test"] = self::test_get($test_id, false);
		}
		$wpdb->update("bio_result",
			[
				"right_count" => $right_count,
				"credits" => $credits,
			],
			["id" => $result_id]
		);
		
		return $results;
	}



}