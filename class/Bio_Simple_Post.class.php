<?php 

class Bio_Simple_Post extends SMC_Post
{
	static function get_type()
	{
		return BIO_SIMLE_POST_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 2);	
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Simple Post", BIO), // Основное название типа записи
			'singular_name'      => __("Simple Post", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Simple Post", BIO), 
			'all_items' 		 => __('Simple Posts', BIO),
			'add_new_item'       => __("add Simple Post", BIO), 
			'edit_item'          => __("edit Simple Post", BIO), 
			'new_item'           => __("add Simple Post", BIO), 
			'view_item'          => __("see Simple Post", BIO), 
			'search_items'       => __("search Simple Post", BIO), 
			'not_found'          => __("no Simple Posts", BIO), 
			'not_found_in_trash' => __("no Simple Posts in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Simple Posts", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 4,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
}